﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System.Net.Mail;


namespace CostAndPriceUpdate
{
    public static class LibraryUtil
    {
        // Create Error Log File

        #region Depricated Exception Handling common Log File to be called on Every Exception Area
        public static void LogExceptions(string strErrorMessage, string strStacktrace, string ExceptionArea, Boolean Bool)
        {
            string strErrorLogFileFullPath = string.Empty;
            try
            {
                strErrorLogFileFullPath = ConfigurationManager.AppSettings["ErrorLogFilePath"].ToString();

                if (!File.Exists(strErrorLogFileFullPath))
                {
                    File.Create(strErrorLogFileFullPath);
                    TextWriter TextWriteError = new StreamWriter(strErrorLogFileFullPath, true);
                    TextWriteError.WriteLine("-------------------- Start of Error Log-----------------------");
                    TextWriteError.WriteLine("On: " + DateTime.Now.ToString() + ": Error Message : " + strErrorMessage);
                    TextWriteError.WriteLine("On: " + DateTime.Now.ToString() + ": Error Stack : " + strStacktrace);
                    TextWriteError.WriteLine("On: " + DateTime.Now.ToString() + ": Error Area : " + ExceptionArea);
                    TextWriteError.Close();
                }
                else
                {
                    TextWriter TextWriteError = new StreamWriter(strErrorLogFileFullPath, true);
                    TextWriteError.WriteLine("On: " + DateTime.Now.ToString() + ":Error Message : " + strErrorMessage);
                    TextWriteError.WriteLine("On: " + DateTime.Now.ToString() + ":Error Stack : " + strStacktrace);
                    TextWriteError.WriteLine("On: " + DateTime.Now.ToString() + ":Error Area : " + ExceptionArea);
                    TextWriteError.Close();
                }

            }
            catch (Exception exc)
            {
                // Handle to send Email for developer
            }

        }
        #endregion

        #region Exception Handling common Log File to be called on Every Exception Area
        public static void LogExceptions(string strErrorMessage, string strStacktrace, string ExceptionArea)
        {
            string strErrorLogFileFullPath = string.Empty;
            try
            {
                strErrorLogFileFullPath = ConfigurationManager.AppSettings["PartialErrorLogFilePath"].ToString();

                strErrorLogFileFullPath = strErrorLogFileFullPath + DateTime.Today.ToString("ddMMyyyy") + ".log";

                using (var TextWriteError = new StreamWriter(strErrorLogFileFullPath, true))
                {

                    TextWriteError.WriteLine("-------------------- Start of Error Log-----------------------");
                    TextWriteError.WriteLine("On: " + DateTime.Now.ToString() + ": Error Message : " + strErrorMessage);
                    TextWriteError.WriteLine("On: " + DateTime.Now.ToString() + ": Error Stack : " + strStacktrace);
                    TextWriteError.WriteLine("On: " + DateTime.Now.ToString() + ": Error Area : " + ExceptionArea);
                    TextWriteError.WriteLine("-------------------- End of Error Log-----------------------");
                }


            }
            catch (Exception exc)
            {
                // Handle to send Email for developer
            }

        }
        #endregion


        // Log File to collect each activities
        #region Log Each steps of activities with Current Datetime File name in particular path
        public static void LogActivities(string strLogDetailStep)
        {
            string strLogFileFullPath = string.Empty;
            try
            {
                strLogFileFullPath = ConfigurationManager.AppSettings["PartialLogFilePath"].ToString();
                strLogFileFullPath = strLogFileFullPath + DateTime.Today.ToString("ddMMyyyy") + ".log";

                using (var TextWriteError = new StreamWriter(strLogFileFullPath, true))
                {
                    TextWriteError.WriteLine("On: " + DateTime.Now.ToString() + strLogDetailStep);

                }
            }
            catch (Exception exc)
            {
                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " Occured in LogActivities Method";

                LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }

        }
        #endregion

        // Log File to collect each activities with Thread Call
        #region Log Each steps of activities with Current Datetime File name in particular path
        public static void LogActivities(string strLogDetailStep, string strLogFilewithThread)
        {
            string strLogFileFullPath = string.Empty;
            try
            {
                strLogFileFullPath = ConfigurationManager.AppSettings["PartialLogFilePath"].ToString();
                strLogFileFullPath = strLogFileFullPath + DateTime.Today.ToString("ddMMyyyy") + "-" + strLogFilewithThread + ".log";

                using (var TextWriteError = new StreamWriter(strLogFileFullPath, true))
                {
                    TextWriteError.WriteLine("On: " + strLogDetailStep);

                }
            }
            catch (Exception exc)
            {
                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " Occured in LogActivities Method";

                LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }

        }
        #endregion
       
        #region Log Each steps of activities with Current Datetime File name in particular path
        public static void LogActivitiesForChangeTime(string strLogDetailStep, string strLogFilewithThread)
        {
            string strLogFileFullPath = string.Empty;
            try
            {
                strLogFileFullPath = ConfigurationManager.AppSettings["PartialLogFilePathForScheduleChange"].ToString();
                strLogFileFullPath = strLogFileFullPath + DateTime.Today.ToString("ddMMyyyy") + "-" + strLogFilewithThread + ".log";

                using (var TextWriteError = new StreamWriter(strLogFileFullPath, true))
                {
                    TextWriteError.WriteLine("On: " + strLogDetailStep);

                }
            }
            catch (Exception exc)
            {
                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " Occured in LogActivities Method";

                LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }

        }
        #endregion

        #region Log Each steps of activities with Current Datetime File name in particular path
        public static void LogActivitiesForFlightDefault(string strLogDetailStep, string strLogFilewithThread)
        {
            string strLogFileFullPath = string.Empty;
            try
            {
                strLogFileFullPath = ConfigurationManager.AppSettings["PartialLogFilePathForDefaultOption"].ToString();
                strLogFileFullPath = strLogFileFullPath + DateTime.Today.ToString("ddMMyyyy") + "-" + strLogFilewithThread + ".log";

                using (var TextWriteError = new StreamWriter(strLogFileFullPath, true))
                {
                    TextWriteError.WriteLine("On: " + strLogDetailStep);

                }
            }
            catch (Exception exc)
            {
                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public static void LogActivitiesForFlightDefault(string strLogDetailStep, string strLogFilewithThread)";

                LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }

        }
        #endregion

        #region Log Each steps of activities with Current Datetime File name in particular path
        public static void LogActivitiesinCSVForTable(string strLogDetailStep, string strLogFilewithThread)
        {
            string strLogFileFullPath = string.Empty;
            try
            {
                strLogFileFullPath = ConfigurationManager.AppSettings["PartialLogCSVFilePath"].ToString();
                strLogFileFullPath = strLogFileFullPath + DateTime.Today.ToString("ddMMyyyymm") + "-" + strLogFilewithThread + ".log";

                using (var TextWriteError = new StreamWriter(strLogFileFullPath, true))
                {
                    TextWriteError.WriteLine("On: " + strLogDetailStep);

                }
            }
            catch (Exception exc)
            {
                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " Occured in LogActivities Method";

                LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }

        }
        #endregion


        public static IWebElement FindElementOnPage(this IWebDriver webDriver, By by)
        {
            RemoteWebElement element = (RemoteWebElement)webDriver.FindElement(by);
            var hack = element.LocationOnScreenOnceScrolledIntoView;
            return element;
        }

        #region This method will take input paramatert of type Log or Error and Send email
        public static void SendLogEmail(string strLogType)
        {
            try
            {
                string strSMTPServer = string.Empty;
                string strFromEmailAddress = string.Empty;
                string strToEmailAddress = string.Empty;
                string strSMTLEmailAddress = string.Empty;
                string strSMTPEmailPassword = string.Empty;
                string STMPPort = string.Empty;

                string strSubject = string.Empty;
                string strBody = string.Empty;
                string strEmailBodyPath = string.Empty;
                List<string> LststrAttachmentFullPathandFileName = new List<string>();

                // get All SMTP COnfig details from Config File

                strSMTPServer = ConfigurationManager.AppSettings["SMTPServer"].ToString();
                strFromEmailAddress = ConfigurationManager.AppSettings["LogFromAddress"].ToString();
                strSMTLEmailAddress = ConfigurationManager.AppSettings["EmailSendUserName"].ToString();
                strSMTPEmailPassword = ConfigurationManager.AppSettings["EmailSendPassword"].ToString();
                STMPPort = ConfigurationManager.AppSettings["LogEmailServerPort"].ToString();
                // <add key="LogToEmailLists" value="vijay.kanaginhal@newmarketholidays.co.uk"/>


                #region get all attachment from path
                string strFolderPathForLogs = string.Empty;




                #endregion



                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(strSMTPServer);

                if (strLogType.ToLower().Contains("l"))
                {
                    strToEmailAddress = ConfigurationManager.AppSettings["LogToEmailLists"].ToString();
                    strFolderPathForLogs = ConfigurationManager.AppSettings["LogFilePath"].ToString();
                    //strSubject = "Log for Cost and PriceUpdate";
                    //strBody = "Log file for cost and price update on daily basis.";
                    //<add key="StatusEmailSubject" value="Automation Job: Change costs/Prices is Complete" />
                    strSubject = ConfigurationManager.AppSettings["StatusEmailSubject"].ToString();
                    //work For Body Email

                    string strEmailBodyFile = ConfigurationManager.AppSettings["EmailBody"].ToString();

                    strEmailBodyFile = strEmailBodyFile + "-" + DateTime.Today.ToString("ddMMyyyy") + ".log";

                    using (var TextReadBody = new StreamReader(strEmailBodyFile))
                    {
                        strEmailBodyPath = TextReadBody.ReadToEnd();
                    }

                    strBody = strEmailBodyPath;

                    if (Directory.Exists(strFolderPathForLogs))
                    {
                        string[] AryFilePath = Directory.GetFiles(strFolderPathForLogs);
                        foreach (string strFillFilePath in AryFilePath)
                        {
                            Attachment Atch = new Attachment(strFillFilePath);
                            mail.Attachments.Add(Atch);

                        }

                    }
                }
                else if (strLogType.ToLower().Contains("e"))
                {
                    strToEmailAddress = ConfigurationManager.AppSettings["ErrorToEmailLists"].ToString();
                    strFolderPathForLogs = ConfigurationManager.AppSettings["ErrorLogFilePath"].ToString();
                    strSubject = "Error Log for Cost and PriceUpdate";
                    strBody = "Error Log file for cost and price update on daily basis.";


                    if (Directory.Exists(strFolderPathForLogs))
                    {
                        string[] AryFilePath = Directory.GetFiles(strFolderPathForLogs);
                        foreach (string strFillFilePath in AryFilePath)
                        {
                            Attachment Atch = new Attachment(strFillFilePath);
                            mail.Attachments.Add(Atch);

                        }

                    }
                }
                else
                {
                    strToEmailAddress = "vijay.kanaginhal@newmarketholidays.co.uk";
                }

                mail.From = new MailAddress(strFromEmailAddress);
                foreach (var address in strToEmailAddress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mail.To.Add(address);
                }
                //mail.To.Add(strToEmailAddress);
                mail.Subject = strSubject;
                mail.Body = strBody;

                SmtpServer.Port = Convert.ToInt32(STMPPort);
                SmtpServer.Credentials = new System.Net.NetworkCredential(strSMTLEmailAddress, strSMTPEmailPassword);
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);


                mail.Dispose();
                SmtpServer.Dispose();

                // Move Log File to Archive Folder
                if (strLogType.ToLower().Contains("l"))
                {
                    strFolderPathForLogs = ConfigurationManager.AppSettings["LogFilePath"].ToString();
                    //LogArchiveFilePath
                    string strFolderPathForArchiveLogs = ConfigurationManager.AppSettings["LogArchiveFilePath"].ToString();


                    if (Directory.Exists(strFolderPathForLogs))
                    {
                        string[] AryFilePath = Directory.GetFiles(strFolderPathForLogs);
                        foreach (string strFillFilePath in AryFilePath)
                        {
                            string strFileName = Path.GetFileName(strFillFilePath);
                            //strFolderPathForArchiveLogs = strFolderPathForArchiveLogs + strFileName;
                            string strFinalFile = strFolderPathForArchiveLogs + strFileName;
                            File.Move(strFillFilePath, strFinalFile);
                        }

                    }
                }

                // Move Error Log File to Archive Folder
                if (strLogType.ToLower().Contains("e"))
                {
                    strFolderPathForLogs = ConfigurationManager.AppSettings["ErrorLogFilePath"].ToString();
                    //LogArchiveFilePath
                    string strFolderPathForArchiveLogs = ConfigurationManager.AppSettings["LogArchiveFilePath"].ToString();


                    if (Directory.Exists(strFolderPathForLogs))
                    {
                        string[] AryFilePath = Directory.GetFiles(strFolderPathForLogs);
                        foreach (string strFillFilePath in AryFilePath)
                        {
                            string strFileName = Path.GetFileName(strFillFilePath);
                            //strFolderPathForArchiveLogs = strFolderPathForArchiveLogs + strFileName;
                            string strFinalFile = strFolderPathForArchiveLogs + strFileName;
                            File.Move(strFillFilePath, strFinalFile);
                        }

                    }
                }
            }
            catch (Exception exc)
            {
                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public static void SendEmail()";

                LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }



        }
        #endregion

        #region This method will take input paramatert of type Log or Error and Send email
        public static void SendLogEmail(string strLogType,string strTypeOfAutomation)
        {
            try
            {
                string strSMTPServer = string.Empty;
                string strFromEmailAddress = string.Empty;
                string strToEmailAddress = string.Empty;
                string strSMTLEmailAddress = string.Empty;
                string strSMTPEmailPassword = string.Empty;
                string STMPPort = string.Empty;

                string strSubject = string.Empty;
                string strBody = string.Empty;
                string strEmailBodyPath = string.Empty;
                List<string> LststrAttachmentFullPathandFileName = new List<string>();

                // get All SMTP COnfig details from Config File

                strSMTPServer = ConfigurationManager.AppSettings["SMTPServer"].ToString();
                strFromEmailAddress = ConfigurationManager.AppSettings["LogFromAddress"].ToString();
                strSMTLEmailAddress = ConfigurationManager.AppSettings["EmailSendUserName"].ToString();
                strSMTPEmailPassword = ConfigurationManager.AppSettings["EmailSendPassword"].ToString();
                STMPPort = ConfigurationManager.AppSettings["LogEmailServerPort"].ToString();
                // <add key="LogToEmailLists" value="vijay.kanaginhal@newmarketholidays.co.uk"/>


                #region get all attachment from path
                string strFolderPathForLogs = string.Empty;




                #endregion



                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(strSMTPServer);

                if (strLogType.ToLower().Contains("l"))
                {
                    strToEmailAddress = ConfigurationManager.AppSettings["LogToEmailLists"].ToString();
                    strFolderPathForLogs = ConfigurationManager.AppSettings["LogFilePath"].ToString();
                    //strSubject = "Log for Cost and PriceUpdate";
                    //strBody = "Log file for cost and price update on daily basis.";
                    //<add key="StatusEmailSubject" value="Automation Job: Change costs/Prices is Complete" />
                    if (strTypeOfAutomation == "time")
                    {
                        strSubject = ConfigurationManager.AppSettings["StatusEmailSubjectForScheduleTime"].ToString();
                    }
                    else if(strTypeOfAutomation == "Default")
                    {
                        strSubject = ConfigurationManager.AppSettings["StatusEmailSubjectForDefaultOption"].ToString();
                    }
                    else
                    {
                        strSubject = ConfigurationManager.AppSettings["StatusEmailSubject"].ToString();
                    }
                        //work For Body Email

                    string strEmailBodyFile = ConfigurationManager.AppSettings["EmailBody"].ToString();

                    strEmailBodyFile = strEmailBodyFile + "-" + DateTime.Today.ToString("ddMMyyyy") + ".log";

                    using (var TextReadBody = new StreamReader(strEmailBodyFile))
                    {
                        strEmailBodyPath = TextReadBody.ReadToEnd();
                    }

                    strBody = strEmailBodyPath;

                    if (Directory.Exists(strFolderPathForLogs))
                    {
                        string[] AryFilePath = Directory.GetFiles(strFolderPathForLogs);
                        foreach (string strFillFilePath in AryFilePath)
                        {
                            Attachment Atch = new Attachment(strFillFilePath);
                            mail.Attachments.Add(Atch);

                        }

                    }
                }
                else if (strLogType.ToLower().Contains("e"))
                {
                    strToEmailAddress = ConfigurationManager.AppSettings["ErrorToEmailLists"].ToString();
                    strFolderPathForLogs = ConfigurationManager.AppSettings["ErrorLogFilePath"].ToString();
                    strSubject = "Error Log for Schedule Time change";
                    strBody = "Error Log file for Schedule Time change on daily basis.";


                    if (Directory.Exists(strFolderPathForLogs))
                    {
                        string[] AryFilePath = Directory.GetFiles(strFolderPathForLogs);
                        foreach (string strFillFilePath in AryFilePath)
                        {
                            Attachment Atch = new Attachment(strFillFilePath);
                            mail.Attachments.Add(Atch);

                        }

                    }
                }
                else
                {
                    strToEmailAddress = "vijay.kanaginhal@newmarketholidays.co.uk";
                }

                mail.From = new MailAddress(strFromEmailAddress);
                foreach (var address in strToEmailAddress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mail.To.Add(address);
                }
                //mail.To.Add(strToEmailAddress);
                mail.Subject = strSubject;
                mail.Body = strBody;

                SmtpServer.Port = Convert.ToInt32(STMPPort);
                SmtpServer.Credentials = new System.Net.NetworkCredential(strSMTLEmailAddress, strSMTPEmailPassword);
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);


                mail.Dispose();
                SmtpServer.Dispose();

                // Move Log File to Archive Folder
                //############################################
                //############################################
                if (strLogType.ToLower().Contains("l"))
                {
                    strFolderPathForLogs = ConfigurationManager.AppSettings["LogFilePath"].ToString();
                    //LogArchiveFilePath
                    string strFolderPathForArchiveLogs = string.Empty;
                    if (strTypeOfAutomation == "time")
                    {
                        strFolderPathForArchiveLogs = ConfigurationManager.AppSettings["LogArchiveFilePathScheduleTime"].ToString();

                    }
                    else if (strTypeOfAutomation == "Default")
                    {
                        strFolderPathForArchiveLogs = ConfigurationManager.AppSettings["LogArchiveFilePathDefaultOption"].ToString();
                    }
                    else
                    {
                        strFolderPathForArchiveLogs = ConfigurationManager.AppSettings["LogArchiveFilePath"].ToString();
                    }
                       

                    if (Directory.Exists(strFolderPathForLogs))
                    {
                        string[] AryFilePath = Directory.GetFiles(strFolderPathForLogs);
                        foreach (string strFillFilePath in AryFilePath)
                        {
                            string strFileName = Path.GetFileName(strFillFilePath);
                            //strFolderPathForArchiveLogs = strFolderPathForArchiveLogs + strFileName;
                            string strFinalFile = strFolderPathForArchiveLogs + strFileName;
                            File.Move(strFillFilePath, strFinalFile);
                        }

                    }
                }

                // Move Error Log File to Archive Folder
                if (strLogType.ToLower().Contains("e"))
                {
                    strFolderPathForLogs = ConfigurationManager.AppSettings["ErrorLogFilePath"].ToString();
                    //LogArchiveFilePath
                    //string strFolderPathForArchiveLogs = ConfigurationManager.AppSettings["LogArchiveFilePath"].ToString();
                    string strFolderPathForArchiveLogs = ConfigurationManager.AppSettings["LogArchiveFilePathScheduleTime"].ToString();


                    if (Directory.Exists(strFolderPathForLogs))
                    {
                        string[] AryFilePath = Directory.GetFiles(strFolderPathForLogs);
                        foreach (string strFillFilePath in AryFilePath)
                        {
                            string strFileName = Path.GetFileName(strFillFilePath);
                            //strFolderPathForArchiveLogs = strFolderPathForArchiveLogs + strFileName;
                            string strFinalFile = strFolderPathForArchiveLogs + strFileName;
                            File.Move(strFillFilePath, strFinalFile);
                        }

                    }
                }
            }
            catch (Exception exc)
            {
                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public static void SendEmail()";

                LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }



        }
        #endregion

        #region Depricated Method
        public static void LogSendSendEmail()
        {
            try
            {
                string strSMTPServer = string.Empty;
                string strFromEmailAddress = string.Empty;
                string strToEmailAddress = string.Empty;
                string strSMTLEmailAddress = string.Empty;
                string strSMTPEmailPassword = string.Empty;
                string STMPPort = string.Empty;

                string strSubject = string.Empty;
                string strBody = string.Empty;
                List<string> LststrAttachmentFullPathandFileName = new List<string>();

                // get All SMTP COnfig details from Config File

                strSMTPServer = ConfigurationManager.AppSettings["SMTPServer"].ToString();
                strFromEmailAddress = ConfigurationManager.AppSettings["LogFromAddress"].ToString();
                strSMTLEmailAddress = ConfigurationManager.AppSettings["EmailSendUserName"].ToString();
                strSMTPEmailPassword = ConfigurationManager.AppSettings["EmailSendPassword"].ToString();
                STMPPort = ConfigurationManager.AppSettings["LogEmailServerPort"].ToString();
                // <add key="LogToEmailLists" value="vijay.kanaginhal@newmarketholidays.co.uk"/>
                strToEmailAddress = ConfigurationManager.AppSettings["LogToEmailLists"].ToString();

                #region get all attachment from path
                string strFolderPathForLogs = string.Empty;

                strFolderPathForLogs = ConfigurationManager.AppSettings["LogFilePath"].ToString();



                #endregion



                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(strSMTPServer);


                if (Directory.Exists(strFolderPathForLogs))
                {
                    string[] AryFilePath = Directory.GetFiles(strFolderPathForLogs);
                    foreach (string strFillFilePath in AryFilePath)
                    {
                        Attachment Atch = new Attachment(strFillFilePath);
                        mail.Attachments.Add(Atch);

                    }

                }

                mail.From = new MailAddress(strFromEmailAddress);
                foreach (var address in strToEmailAddress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mail.To.Add(address);
                }
                //mail.To.Add(strToEmailAddress);
                mail.Subject = "Test Mail";
                mail.Body = "This is for testing SMTP mail from smtp.sendgrid.net";

                SmtpServer.Port = Convert.ToInt32(STMPPort);
                SmtpServer.Credentials = new System.Net.NetworkCredential(strSMTLEmailAddress, strSMTPEmailPassword);
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);

            }
            catch (Exception exc)
            {
                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public static void SendEmail()";

                LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }
        }
        #endregion

        #region send mail with Log File attached and Status
        public static void SendEmailBodyWithStatus()
        {
            try
            {
                string strSMTPServer = string.Empty;
                string strFromEmailAddress = string.Empty;
                string strToEmailAddress = string.Empty;
                string strSMTLEmailAddress = string.Empty;
                string strSMTPEmailPassword = string.Empty;
                string STMPPort = string.Empty;
                string strEmailBodyPath = string.Empty;

                string strSubject = string.Empty;
                string strBody = string.Empty;
                List<string> LststrAttachmentFullPathandFileName = new List<string>();

                // get All SMTP COnfig details from Config File

                strSMTPServer = ConfigurationManager.AppSettings["SMTPServer"].ToString();
                strFromEmailAddress = ConfigurationManager.AppSettings["LogFromAddress"].ToString();
                strSMTLEmailAddress = ConfigurationManager.AppSettings["EmailSendUserName"].ToString();
                strSMTPEmailPassword = ConfigurationManager.AppSettings["EmailSendPassword"].ToString();
                STMPPort = ConfigurationManager.AppSettings["LogEmailServerPort"].ToString();
                // <add key="LogToEmailLists" value="vijay.kanaginhal@newmarketholidays.co.uk"/>


                #region get all attachment from path
                string strFolderPathForLogs = string.Empty;




                #endregion



                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(strSMTPServer);


                strToEmailAddress = ConfigurationManager.AppSettings["StatusToEmailLists"].ToString();
                strFolderPathForLogs = ConfigurationManager.AppSettings["LogFilePath"].ToString();
                strSubject = "Automation Job: Change costs/Prices is Complete";


                string strEmailBodyFile = ConfigurationManager.AppSettings["EmailBody"].ToString();

                strEmailBodyFile = strEmailBodyFile + "-" + DateTime.Today.ToString("ddMMyyyy") + ".log";

                using (var TextReadBody = new StreamReader(strEmailBodyFile))
                {
                    strEmailBodyPath = TextReadBody.ReadToEnd();
                }




                mail.From = new MailAddress(strFromEmailAddress);
                foreach (var address in strToEmailAddress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mail.To.Add(address);
                }
                //mail.To.Add(strToEmailAddress);
                mail.Subject = strSubject;
                mail.Body = strEmailBodyPath;

                SmtpServer.Port = Convert.ToInt32(STMPPort);
                SmtpServer.Credentials = new System.Net.NetworkCredential(strSMTLEmailAddress, strSMTPEmailPassword);
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);


                mail.Dispose();
                SmtpServer.Dispose();

                // Move Log file to Archive Folder
                string strFolderPathForArchiveLogs = ConfigurationManager.AppSettings["LogArchiveFilePath"].ToString();

                if (File.Exists(strEmailBodyFile))
                {
                    string strDestinationFileWithPath = strFolderPathForArchiveLogs + Path.GetFileName(strEmailBodyFile);

                    File.Move(@strEmailBodyFile, @strDestinationFileWithPath);

                }
            }
            catch (Exception exc)
            {
                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public static void SendEmail()";

                LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }
        }
        #endregion

        #region send mail with Log File attached and Status
        public static void SendEmailBodyWithStatus(string strAutomationType)
        {
            try
            {
                string strSMTPServer = string.Empty;
                string strFromEmailAddress = string.Empty;
                string strToEmailAddress = string.Empty;
                string strSMTLEmailAddress = string.Empty;
                string strSMTPEmailPassword = string.Empty;
                string STMPPort = string.Empty;
                string strEmailBodyPath = string.Empty;

                string strSubject = string.Empty;
                string strBody = string.Empty;
                List<string> LststrAttachmentFullPathandFileName = new List<string>();

                // get All SMTP COnfig details from Config File

                strSMTPServer = ConfigurationManager.AppSettings["SMTPServer"].ToString();
                strFromEmailAddress = ConfigurationManager.AppSettings["LogFromAddress"].ToString();
                strSMTLEmailAddress = ConfigurationManager.AppSettings["EmailSendUserName"].ToString();
                strSMTPEmailPassword = ConfigurationManager.AppSettings["EmailSendPassword"].ToString();
                STMPPort = ConfigurationManager.AppSettings["LogEmailServerPort"].ToString();
                // <add key="LogToEmailLists" value="vijay.kanaginhal@newmarketholidays.co.uk"/>


                #region get all attachment from path
                string strFolderPathForLogs = string.Empty;




                #endregion



                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(strSMTPServer);


                strToEmailAddress = ConfigurationManager.AppSettings["StatusToEmailLists"].ToString();
                strFolderPathForLogs = ConfigurationManager.AppSettings["LogFilePath"].ToString();
                if (strAutomationType.Contains("time"))
                {
                    strSubject = ConfigurationManager.AppSettings["StatusEmailSubjectForScheduleTime"].ToString();
                }
                else if(strAutomationType.Contains("Default"))
                {
                    strSubject = ConfigurationManager.AppSettings["StatusEmailSubjectForDefaultOption"].ToString();
                }
                else
                {
                    strSubject = ConfigurationManager.AppSettings["StatusEmailSubject"].ToString();
                }


                string strEmailBodyFile = ConfigurationManager.AppSettings["EmailBody"].ToString();

                strEmailBodyFile = strEmailBodyFile + "-" + DateTime.Today.ToString("ddMMyyyy") + ".log";

                using (var TextReadBody = new StreamReader(strEmailBodyFile))
                {
                    strEmailBodyPath = TextReadBody.ReadToEnd();
                }




                mail.From = new MailAddress(strFromEmailAddress);
                foreach (var address in strToEmailAddress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mail.To.Add(address);
                }
                //mail.To.Add(strToEmailAddress);
                mail.Subject = strSubject;
                mail.Body = strEmailBodyPath;

                SmtpServer.Port = Convert.ToInt32(STMPPort);
                SmtpServer.Credentials = new System.Net.NetworkCredential(strSMTLEmailAddress, strSMTPEmailPassword);
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);


                mail.Dispose();
                SmtpServer.Dispose();

                // Move Log file to Archive Folder
                //string strFolderPathForArchiveLogs = ConfigurationManager.AppSettings["LogArchiveFilePath"].ToString();

                string strFolderPathForArchiveLogs = string.Empty;
                if (strAutomationType.Contains("time"))
                {
                     strFolderPathForArchiveLogs = ConfigurationManager.AppSettings["LogArchiveFilePathScheduleTime"].ToString();
                }
                else
                {
                     strFolderPathForArchiveLogs = ConfigurationManager.AppSettings["LogArchiveFilePathDefaultOption"].ToString();
                }
                if (File.Exists(strEmailBodyFile))
                {
                    string strDestinationFileWithPath = strFolderPathForArchiveLogs + Path.GetFileName(strEmailBodyFile);

                    File.Move(@strEmailBodyFile, @strDestinationFileWithPath);

                }
            }
            catch (Exception exc)
            {
                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public static void SendEmail()";

                LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }
        }
        #endregion
        #region Just Email Body
        public static void EmailBody(string strContent)
        {
            try
            {
                string strEmailBodyFile = ConfigurationManager.AppSettings["EmailBody"].ToString();

                strEmailBodyFile = strEmailBodyFile + "-" + DateTime.Today.ToString("ddMMyyyy") + ".log";

                using (var TextWriteBody = new StreamWriter(strEmailBodyFile, true))
                {
                    TextWriteBody.WriteLine(strContent.ToString() + Environment.NewLine);
                }
            }
            catch (Exception exc)
            {
                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public static void EmailBody(string strContent)";

                LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }
        }
        #endregion
    }
}
