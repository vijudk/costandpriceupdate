﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using Renci.SshNet;

namespace CostAndPriceUpdate
{
    class FTPDownLoadMoveDelete
    {
        #region Download File from FTP Server
        public string ProcessFTPDownload()
        {
            string strFTPHost = string.Empty;// @"newmarketholidays.ftp.uk";
            string strFTPUserName = string.Empty;//"automate.robot";
            string strFTPPassword = string.Empty;//"558W2ZcN";
            string strFTPPath = string.Empty;

            string strReturnPath = string.Empty;

            //      < add key = "FTPPAth" value = "home/paxport-internal/Archive/" />

            //< add key = "FTPHost" value = "newmarketholidays.ftp.uk" />

            //  < add key = "FTPUserName" value = "automate.robot" />

            //    < add key = "FTPPassword" value = "558W2ZcN" />

            strFTPHost = @ConfigurationManager.AppSettings["FTPHost"].ToString();
            strFTPUserName = @ConfigurationManager.AppSettings["FTPUserName"].ToString();
            strFTPPassword = @ConfigurationManager.AppSettings["FTPPassword"].ToString();
            strFTPPath = @ConfigurationManager.AppSettings["FTPPAth"].ToString();

            //string localDirectory = @"C:\Vijay\Working Folder\Sample Work\BrowserAutomation\FTPFiles\";
            //LocalFTPDownload
            string localDirectory = @ConfigurationManager.AppSettings["LocalFTPDownload"].ToString();

            try
            {
                string strDateFolder = DateTime.Now.ToString("ddMMyyyy");
                Boolean FileExistinServer = false;
                localDirectory = localDirectory + strDateFolder + @"\\";
                if (!Directory.Exists(localDirectory))
                {
                    Directory.CreateDirectory(localDirectory);
                }

                using (var sftp = new SftpClient(strFTPHost, strFTPUserName, strFTPPassword))
                {
                    sftp.Connect();
                    //var files = sftp.ListDirectory(sftp.WorkingDirectory);
                    var files = sftp.ListDirectory(sftp.WorkingDirectory + @strFTPPath);
                    string remoteDirectory = @sftp.WorkingDirectory + @strFTPPath;

                    foreach (var file in files)
                    {
                        string strFilename = file.FullName;
                        DateTime dtLastModifiedDT = file.LastWriteTime;
                        if ((DateTime.Now.ToString("ddMMyyyy")) == dtLastModifiedDT.ToString("ddMMyyyy"))
                        {
                            localDirectory = @localDirectory + file.Name;
                            using (Stream file1 = File.OpenWrite(@localDirectory))
                            {
                                sftp.DownloadFile(strFilename, file1);
                                FileExistinServer = true;
                            }
                        }



                    }

                    if (FileExistinServer)
                    {
                        strReturnPath = @localDirectory;
                    }
                    else
                    {
                        strReturnPath = string.Empty;
                    }
                    //if ((!file.Name.StartsWith(".")) && ((file.LastWriteTime.Date == DateTime.Today)))

                    //    using (Stream file1 = File.OpenWrite(localDirectory + remoteFileName))
                    //    {
                    //        sftp.DownloadFile(remoteDirectory + remoteFileName, file1);
                    //    }


                }
            }
            catch (Exception exc)
            {  // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public void ProcessFTPDownload()";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }
            return strReturnPath;
        }
        #endregion

        #region Move the Downloaded File to Archive Folder and Delete from Mail Download Path
        public void MoveFileAndDeleteFromFTPPAth()
        {

            string strFTPHost = string.Empty;// @"newmarketholidays.ftp.uk";
            string strFTPUserName = string.Empty;//"automate.robot";
            string strFTPPassword = string.Empty;//"558W2ZcN";
            string strFTPPath = string.Empty;
            string strFTPArchivePath = string.Empty;


            //      < add key = "FTPPAth" value = "home/paxport-internal/Archive/" />

            //< add key = "FTPHost" value = "newmarketholidays.ftp.uk" />

            //  < add key = "FTPUserName" value = "automate.robot" />

            //    < add key = "FTPPassword" value = "558W2ZcN" />

            strFTPHost = @ConfigurationManager.AppSettings["FTPHost"].ToString();
            strFTPUserName = @ConfigurationManager.AppSettings["FTPUserName"].ToString();
            strFTPPassword = @ConfigurationManager.AppSettings["FTPPassword"].ToString();
            strFTPPath = @ConfigurationManager.AppSettings["FTPPAth"].ToString();
            //FTPArchive
            strFTPArchivePath = @ConfigurationManager.AppSettings["FTPArchive"].ToString();

            //string localDirectory = @"C:\Vijay\Working Folder\Sample Work\BrowserAutomation\FTPFiles\";
            //LocalFTPDownload
            string localDirectory = @ConfigurationManager.AppSettings["LocalFTPDownload"].ToString();
            try
            {
                using (var sftp = new SftpClient(strFTPHost, strFTPUserName, strFTPPassword))
                {
                    sftp.Connect();
                    //var files = sftp.ListDirectory(sftp.WorkingDirectory);
                    var files = sftp.ListDirectory(sftp.WorkingDirectory + strFTPArchivePath);
                    string remoteDirectory = @sftp.WorkingDirectory + strFTPArchivePath;

                    string strDateFolder = DateTime.Now.ToString("ddMMyyyy");

                    Directory.GetDirectories(localDirectory);
                    localDirectory = localDirectory + strDateFolder + @"\\";

                    foreach (var file in Directory.GetFiles(localDirectory))
                    {
                        string strFilename = Path.GetFileName(file);
                        remoteDirectory = remoteDirectory + @"\\" + strFilename;

                        //localDirectory = @localDirectory + file.Name;


                        using (Stream file1 = File.OpenRead(file))
                        {
                            //sftp.DownloadFile(strFilename, file1);
                            sftp.UploadFile(file1, remoteDirectory);
                        }
                    }
                    // Delete the file once uploaded.
                    var filesDelete = sftp.ListDirectory(sftp.WorkingDirectory + strFTPPath);
                    foreach (var file in Directory.GetFiles(localDirectory))
                    {
                        string strFilename = Path.GetFileName(file);
                        sftp.DeleteFile(file);
                    }

                    //if ((!file.Name.StartsWith(".")) && ((file.LastWriteTime.Date == DateTime.Today)))

                    //    using (Stream file1 = File.OpenWrite(localDirectory + remoteFileName))
                    //    {
                    //        sftp.DownloadFile(remoteDirectory + remoteFileName, file1);
                    //    }


                }
            }
            catch (Exception exc)
            {  // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public void MoveFileAndDeleteFromFTPPAth()";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }

        }
        #endregion
    }
}
