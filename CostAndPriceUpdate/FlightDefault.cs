﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using System.Configuration;
using System.IO;
using Renci.SshNet;
using OpenQA.Selenium.Remote;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CostAndPriceUpdate
{
    public class FlightDefault
    {
        public string strFlightNo { get; set; }
        public string strURLForChangeDefault { get; set; }
        public string strDropDownType { get; set; }
        public string strRowId { get; set; }
    }

    #region Exteded Class based on New View created on 7th Feb 2020
    public class FlightDefaultExt
    {

        public string strURLForChangeDefault { get; set; }
        public string strOptionalRowId { get; set; }
        public string strDefaultRowId { get; set; }

    }
    #endregion


    public class ProcessFlightDefault
    {


        #region Login for givin Application
        public ChromeDriver LoginToApp(string strOrderOfLogin)
        {
            try
            {

                ChromeOptions options = new ChromeOptions();
                options.AcceptInsecureCertificates = true;
                options.AddArgument("--ignore-certificate-errors");
                options.AddArgument("--headless");
                options.AddArgument("--disable-gpu");
                options.AddArgument("--no-startup-window");
                options.UnhandledPromptBehavior = UnhandledPromptBehavior.Accept;


                var chromeDriverService = ChromeDriverService.CreateDefaultService();
                chromeDriverService.HideCommandPromptWindow = true;

                string strLoginURL = ConfigurationManager.AppSettings["TightBayURL"].ToString();

                var increatetimeout = TimeSpan.FromMinutes(3);


                var driver = new ChromeDriver(chromeDriverService, options, increatetimeout);


                string strUserName = "vijay.kanaginhal@newmarketholidays.co.uk";
                string strPassword = "Welcome1!";



                driver.Manage().Window.Maximize();
                driver.Navigate().GoToUrl(@strLoginURL);

                var UserName = driver.FindElementById("Username");
                var Password = driver.FindElementById("Password");

                // ************** Live User Login
                //UserName.SendKeys("robot@newmarket.travel");
                //Password.SendKeys("Welcome1!");

                //***************** Staging User Login details
                //UserName.SendKeys("vijay.kanaginhal@newmarketholidays.co.uk");
                //Password.SendKeys("Welcome1!");

                // Get Username and password from web config, check for LIVE or prod from Config

                string strCheckStagingOrProd = ConfigurationManager.AppSettings["Staging"].ToString();
                if (strCheckStagingOrProd.ToLower().Contains("false"))
                {
                    strUserName = ConfigurationManager.AppSettings["ProductionLogin"].ToString();
                    strPassword = ConfigurationManager.AppSettings["ProductionPassword"].ToString();
                }

                UserName.SendKeys(strUserName);
                Password.SendKeys(strPassword);


                var Login = driver.FindElementByName("button");

                Login.Click();
                LibraryUtil.LogActivitiesForFlightDefault(": Start of Login: for Process :" + strOrderOfLogin.ToString() + "@:" + DateTime.Now, strOrderOfLogin);
                var SessionForTiger = driver.SessionId;

                return driver;

            }
            catch (Exception exc)
            {
                // Handle to Error Log

                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " Application Login Page";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
                return null;

            }


        }
        #endregion

        #region Process Cost and Price update with Given 3 Dimension generic List,with Unique Log No
        public Boolean ProcessAllFlightDefault(ChromeDriver driver, List<FlightDefault> objSlicedList, string LogFileExtn)
        {
            Boolean BoolRetVal = false;


            try
            {






                string strFlightDefaultOptionDrpBx = "cph_col2_cph_content_uiComponents_uxRateOption_";
                string StrSearchURL = string.Empty;

                List<FlightDefault> TotalList = new List<FlightDefault>();
                TotalList = objSlicedList;

                var objLock = new Object();

                //foreach (List<string> LstEachLineVal in TotalList)
                //{

                Int32 intExactSlicedArry = 0;
                intExactSlicedArry = Convert.ToInt32(LogFileExtn);
                int i = 0;

                //foreach (string EachLineVal in LstEachLineVal)
                //foreach (string EachLineVal in TotalList[intExactSlicedArry])
                for (int NoOfRec = 0; NoOfRec < TotalList.Count; NoOfRec++)
                {
                    lock (Thread.CurrentThread)
                    {
                        LibraryUtil.LogActivitiesForFlightDefault(DateTime.Now + "No :( " + i.ToString() + ") ", LogFileExtn);
                        LibraryUtil.LogActivitiesForFlightDefault("---------------------------------", LogFileExtn);

                        StrSearchURL = TotalList[NoOfRec].strURLForChangeDefault.ToString();

                        if (StrSearchURL.Contains("http"))
                        {

                            try
                            {

                                StrSearchURL = ChangeStagingURL(StrSearchURL);
                                driver.Navigate().GoToUrl(StrSearchURL);
                                var FlightDefaultRowNo = TotalList[NoOfRec].strRowId.ToString();
                                var FlightDdefaultExactDropDown = strFlightDefaultOptionDrpBx + FlightDefaultRowNo;

                                //DropDown Control Element
                                var strFlightTimeCheckBxId = driver.FindElementById(FlightDdefaultExactDropDown);

                                var selectElement = new SelectElement(strFlightTimeCheckBxId);
                                //jse.ExecuteScript(strFlightTimeCheckBx,strFlightTimeCheckBxId);
                                //strFlightTimeCheckBxId.Clear();
                                selectElement.SelectByText(TotalList[NoOfRec].strDropDownType.ToString(), true);










                                LibraryUtil.LogActivitiesForFlightDefault("Flight Number : " + TotalList[NoOfRec].strFlightNo, LogFileExtn);
                                LibraryUtil.LogActivitiesForFlightDefault("Page URL : " + TotalList[NoOfRec].strURLForChangeDefault, LogFileExtn);

                                LibraryUtil.LogActivitiesForFlightDefault("Search Successfully", LogFileExtn);


                                #region Proceed part to moveahead
                                string strButtonSubmit = "cph_col2_cph_content_uxSave";
                                var VarProceed = driver.FindElementById(strButtonSubmit);
                                IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
                                jse.ExecuteScript("arguments[0].click()", VarProceed);
                                LibraryUtil.LogActivitiesForFlightDefault("Submited Successfully : ", LogFileExtn);
                                LibraryUtil.LogActivitiesForFlightDefault("-----------------------------------: ", LogFileExtn);

                                #endregion
                            }
                            catch (Exception exc)
                            {
                                // Handle to Error Log
                                string StrErrorMessage = string.Empty;
                                string strStackTrace = string.Empty;
                                string strAreaOfError = string.Empty;

                                StrErrorMessage = "Process No: (" + LogFileExtn + ")" + exc.Message + Environment.NewLine;
                                StrErrorMessage = StrErrorMessage + "ProblemURL: " + ChangeStagingURL(TotalList[NoOfRec].strURLForChangeDefault.ToString()) + Environment.NewLine;
                                StrErrorMessage = StrErrorMessage + "Row Id : " + TotalList[NoOfRec].strRowId.ToString() + Environment.NewLine;
                                strStackTrace = exc.StackTrace.ToString();
                                strAreaOfError = exc.Source + " Occured in Web HyperLink URL Crome Drive";

                                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
                                LibraryUtil.LogActivitiesForFlightDefault("Price Submit Problem", LogFileExtn);

                                // handle timed out Exception
                                if (StrErrorMessage.ToLower().Contains("timed out"))
                                {
                                    FlightSchedureChange ObjbusinessProcess = new FlightSchedureChange();
                                    driver = ObjbusinessProcess.LoginToApp(LogFileExtn);
                                    i--;
                                }
                                // handle Alert Exception 
                                if (StrErrorMessage.ToLower().Contains("unexpected alert"))
                                {
                                    //driver.SwitchTo().Alert().Accept();
                                    //driver = LoginToApp(LogFileExtn);
                                    //i--;
                                }
                            }

                        }
                        // ********************************

                    }


                }
                i++;




                driver.Close();

                CostAndPriceUpdate.IntStatNoOfLoop--;

                LibraryUtil.LogActivitiesForFlightDefault("End Of Process: with following Thread " + LogFileExtn + "@: " + DateTime.Now + " @ : ", LogFileExtn);
                foreach (ClsHoldThreadstate objClsHoldThreadstate in CostAndPriceUpdate.objThreadStatusList)
                {
                    if (Thread.CurrentThread.ManagedThreadId == objClsHoldThreadstate.ThdList)
                    {
                        objClsHoldThreadstate.boolState = false;
                    }
                }

                driver.Quit();

                driver.Dispose();
                // for mail generation

                CheckforEmailSend();
            }
            catch (Exception exc)
            {

                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " ProcessCostPriceUpDate(driver, obj3DimensionList[j])";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

                // report thisvia email

            }

            return BoolRetVal;

        }
        #endregion

        #region Extended Process Cost and Price update with Given 3 Dimension generic List,with Unique Log No
        public Boolean ProcessAllFlightDefaultExt(ChromeDriver driver, List<FlightDefaultExt> objSlicedList, string LogFileExtn)
        {
            Boolean BoolRetVal = false;


            try
            {
                string strFlightDefaultOptionDrpBx = "cph_col2_cph_content_uiComponents_uxRateOption_";
                string StrSearchURL = string.Empty;

                List<FlightDefaultExt> TotalList = new List<FlightDefaultExt>();
                TotalList = objSlicedList;

                var objLock = new Object();


                Int32 intExactSlicedArry = 0;
                intExactSlicedArry = Convert.ToInt32(LogFileExtn);
                int i = 0;

                //foreach (string EachLineVal in LstEachLineVal)
                //foreach (string EachLineVal in TotalList[intExactSlicedArry])
                for (int NoOfRec = 0; NoOfRec < TotalList.Count; NoOfRec++)
                {
                    i++;
                    lock (Thread.CurrentThread)
                    {
                        LibraryUtil.LogActivitiesForFlightDefault(DateTime.Now + "No :( " + i.ToString() + ") ", LogFileExtn);
                        LibraryUtil.LogActivitiesForFlightDefault("---------------------------------", LogFileExtn);
                        LibraryUtil.LogActivitiesForFlightDefault("Page URL : " + TotalList[NoOfRec].strURLForChangeDefault, LogFileExtn);

                        StrSearchURL = TotalList[NoOfRec].strURLForChangeDefault.ToString();

                        if (StrSearchURL.Contains("http"))
                        {

                            try
                            {

                                StrSearchURL = ChangeStagingURL(StrSearchURL);
                                driver.Navigate().GoToUrl(StrSearchURL);

                                #region part 1 work for Optional
                                var FlightOptionalRowNo = TotalList[NoOfRec].strOptionalRowId.ToString();
                                var StrOptionalRowIdNo = FlightOptionalRowNo.Split(',');

                                // loop through No of records and update for default
                                for (int DefRowId = 0; DefRowId < StrOptionalRowIdNo.Length; DefRowId++)
                                {
                                    //int IntDefaultRowIdNo = int.Parse(StrDefaultRowIdNo[DefRowId]);
                                    var FlightOptioalExactDropDown = strFlightDefaultOptionDrpBx + StrOptionalRowIdNo[DefRowId].ToString().Trim();

                                    //DropDown Control Element
                                    var strOptionalDrop = driver.FindElementById(FlightOptioalExactDropDown);

                                    var selectElementOptional = new SelectElement(strOptionalDrop);
                                    //jse.ExecuteScript(strFlightTimeCheckBx,strFlightTimeCheckBxId);
                                    //strFlightTimeCheckBxId.Clear();
                                    //IJavaScriptExecutor jseDropDown = (IJavaScriptExecutor)driver;
                                    //jseDropDown.ExecuteScript("return document.getElementById('id').selectedIndex = '")
                                    selectElementOptional.SelectByText("Optional", true);
                                    LibraryUtil.LogActivitiesForFlightDefault("Drop down setting : Optional", LogFileExtn);
                                    LibraryUtil.LogActivitiesForFlightDefault("Row Id : " + StrOptionalRowIdNo[DefRowId].ToString(), LogFileExtn);

                                }

                                #endregion

                                #region part 2 work for default
                                var FlightDefaultRowNo = TotalList[NoOfRec].strDefaultRowId.ToString();
                                var StrDefaultRowIdNo = FlightDefaultRowNo.Split(',');

                                // loop through No of records and update for default
                                for (int DefRowId = 0; DefRowId < StrDefaultRowIdNo.Length; DefRowId++)
                                {
                                    //int IntDefaultRowIdNo = int.Parse(StrDefaultRowIdNo[DefRowId]);
                                    var FlightDdefaultExactDropDown = strFlightDefaultOptionDrpBx + StrDefaultRowIdNo[DefRowId].ToString().Trim();

                                    //DropDown Control Element
                                    var strDefaultDrop = driver.FindElementById(FlightDdefaultExactDropDown);

                                    var selectElementDefault = new SelectElement(strDefaultDrop);
                                    //jse.ExecuteScript(strFlightTimeCheckBx,strFlightTimeCheckBxId);
                                    //strFlightTimeCheckBxId.Clear();
                                    //IJavaScriptExecutor jseDropDown = (IJavaScriptExecutor)driver;
                                    //jseDropDown.ExecuteScript("return document.getElementById('id').selectedIndex = '")
                                    selectElementDefault.SelectByText("Default", true);
                                    //LibraryUtil.LogActivitiesForFlightDefault("Page URL : " + TotalList[NoOfRec].strURLForChangeDefault, LogFileExtn);
                                    LibraryUtil.LogActivitiesForFlightDefault("Drop down setting : Default ", LogFileExtn);
                                    LibraryUtil.LogActivitiesForFlightDefault("Row Id : " + StrDefaultRowIdNo[DefRowId].ToString(), LogFileExtn);


                                }

                                #endregion


                               


                                //LibraryUtil.LogActivitiesForFlightDefault("Page URL : " + TotalList[NoOfRec].strURLForChangeDefault, LogFileExtn);

                                LibraryUtil.LogActivitiesForFlightDefault("Setting Optional Successfull", LogFileExtn);


                                #region Proceed part to moveahead
                                string strButtonSubmit = "cph_col2_cph_content_uxSave";
                                var VarProceed = driver.FindElementById(strButtonSubmit);
                                IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
                                jse.ExecuteScript("arguments[0].click()", VarProceed);
                                LibraryUtil.LogActivitiesForFlightDefault("Submited Successfully : ", LogFileExtn);
                                LibraryUtil.LogActivitiesForFlightDefault("-----------------------------------: ", LogFileExtn);

                                #endregion
                            }
                            catch (Exception exc)
                            {
                                // Handle to Error Log
                                string StrErrorMessage = string.Empty;
                                string strStackTrace = string.Empty;
                                string strAreaOfError = string.Empty;

                                StrErrorMessage = "Process No: (" + LogFileExtn + ")" + exc.Message + Environment.NewLine;
                                StrErrorMessage = StrErrorMessage + "ProblemURL: " + ChangeStagingURL(TotalList[NoOfRec].strURLForChangeDefault.ToString()) + Environment.NewLine;
                                StrErrorMessage = StrErrorMessage + "Row Id : " + TotalList[NoOfRec].strOptionalRowId.ToString() + Environment.NewLine;
                                strStackTrace = exc.StackTrace.ToString();
                                strAreaOfError = exc.Source + " Occured in Web HyperLink URL Crome Drive";

                                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
                                LibraryUtil.LogActivitiesForFlightDefault("Price Submit Problem", LogFileExtn);

                                // handle timed out Exception
                                if (StrErrorMessage.ToLower().Contains("timed out"))
                                {
                                    FlightSchedureChange ObjbusinessProcess = new FlightSchedureChange();
                                    driver = ObjbusinessProcess.LoginToApp(LogFileExtn);
                                    i--;
                                    NoOfRec--;
                                }
                                // handle Alert Exception 
                                if (StrErrorMessage.ToLower().Contains("unexpected alert"))
                                {
                                    //driver.SwitchTo().Alert().Accept();
                                    //driver = LoginToApp(LogFileExtn);
                                    //i--;
                                }
                            }

                        }
                        // ********************************

                    }


                }
                




                driver.Close();
                CostAndPriceUpdate.IntStatNoOfLoop--;

                LibraryUtil.LogActivitiesForFlightDefault("End Of Process: with following Thread " + LogFileExtn + "@: " + DateTime.Now + " @ : ", LogFileExtn);
                foreach (ClsHoldThreadstate objClsHoldThreadstate in CostAndPriceUpdate.objThreadStatusList)
                {
                    if (Thread.CurrentThread.ManagedThreadId == objClsHoldThreadstate.ThdList)
                    {
                        objClsHoldThreadstate.boolState = false;
                    }
                }

                driver.Quit();

                driver.Dispose();
                // for mail generation

                CheckforEmailSend();
            }
            catch (Exception exc)
            {

                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " ProcessCostPriceUpDate(driver, obj3DimensionList[j])";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

                // report thisvia email

            }

            return BoolRetVal;

        }
        #endregion

        #region This metthod is called only when testing dummy data ( Staging server)
        public string ChangeStagingURL(string strPriceURL)
        {
            string strRetURL = strPriceURL;

            try
            {
                string CheckStaging = ConfigurationManager.AppSettings["Staging"].ToString();
                if (CheckStaging.ToLower().Contains("true"))
                {
                    strRetURL = strPriceURL.Replace("production", "staging");
                }
            }
            catch (Exception exc)
            {  // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public string ChangeStagingURL(string strPriceURL)";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }
            return strRetURL;

        }
        #endregion

        #region check for email to be procesed
        public void CheckforEmailSend()
        {
            Boolean SendStatus = true;

            foreach (ClsHoldThreadstate objClsHoldThreadstate in CostAndPriceUpdate.objThreadStatusList)
            {
                if (objClsHoldThreadstate.boolState)
                {
                    SendStatus = false;
                    break;
                }

            }

            Int32 intNoOfThread = 0;
            intNoOfThread = Convert.ToInt32(ConfigurationManager.AppSettings["NoOfThread"].ToString());

            //if ((SendStatus) &&(CostAndPriceUpdate.IntStatNoOfLoop == 0))
            if (CostAndPriceUpdate.IntStatNoOfLoop == 0)
            {
                // Log the Start of Process
                CostAndPriceUpdate.objThreadStatusList.Clear();
                CostAndPriceUpdate.IntStatNoOfLoop = 0;
                LibraryUtil.EmailBody("Process finished at: " + DateTime.Now);
                // call Static Email Method
                LibraryUtil.SendLogEmail("l", "Default");
                LibraryUtil.SendLogEmail("e", "Default");
                LibraryUtil.SendEmailBodyWithStatus("Default");
            }
        }
        #endregion

        #region Below Method will check for comma in Quotes and replace it with Pipe
        public string CheckFieldEnclosed(string strInput)
        {
            // Check Double Quote and replace, to Pipe
            string strGetStringToReplace = strInput;
            string strReplceString = strInput;
            int intStartInd = 0;
            int intEndIndex = 0;
            int intStartPoint = 0;
            try
            {
                do
                {
                    intStartInd = strInput.IndexOf("\"", intStartPoint);
                    if (intStartInd >= 0)
                    {
                        intEndIndex = strInput.IndexOf("\"", intStartInd + 1);
                        strGetStringToReplace = strInput.Substring(intStartInd, intEndIndex - intStartInd);
                        strInput = strInput.Replace(strGetStringToReplace, strGetStringToReplace.Replace(',', '|'));
                        intStartPoint = intEndIndex + 1;
                    }
                } while (strInput.IndexOf("\"", intStartPoint) > 0);

            }
            catch (Exception exc)
            {
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " Occured in CheckFieldEnclosed(string strInput)";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }
            return strInput;

        }
        #endregion

        #region replace Pipe with Comma
        public string CheckPipeAndRemove(string strCheckPipeAndRemove)
        {
            string strReturn = strCheckPipeAndRemove.Replace('|', ',');

            return strReturn;

        }
        #endregion

        #region replace Pipe with Comma
        public string CheckPipeDoubleQuoteAndRemove(string strCheckPipeAndRemove)
        {
            string strReturn = strCheckPipeAndRemove.Replace('|', ',');
            strReturn = strCheckPipeAndRemove.Replace("\"", "");
            return strReturn;

        }
        #endregion
    }

}
