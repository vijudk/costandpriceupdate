﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Timers;
using System.Configuration;
using Renci.SshNet;
using OpenQA.Selenium.Remote;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;



namespace CostAndPriceUpdate
{
    public partial class CostAndPriceUpdate : ServiceBase
    {
        System.Timers.Timer Systimer = new System.Timers.Timer(); // name space(using System.Timers;) 
        public static List<ClsHoldThreadstate> objThreadStatusList = new List<ClsHoldThreadstate>();
        public static Int32 IntStatNoOfLoop = 0;
        public CostAndPriceUpdate()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {


            try
            {
                // get timer Intervalfrom Config File TimerInterval
                Int32 intTimerIntervalVal = 1;

                intTimerIntervalVal = Convert.ToInt32(ConfigurationManager.AppSettings["TimerInterval"].ToString());
                Systimer.Interval = intTimerIntervalVal * 1000 * 60;
                Systimer.Elapsed += Timer_Elapsed;

                Systimer.Enabled = true;
            }
            catch (Exception exc)
            {  // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " protected override void OnStart(string[] args)";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            // Call timer Method to check if Hourt is between 1 and 1:10 am
            try
            {
                Int32 intTimerIntervalVal = 1;
                Int32 intTimerAtHours = 1;
                Int32 intTimerAtMin = 0;
                Int32 intTimerAtHoursForScheduleChange = 5;
                Int32 intTimerAtMinForScheduleChange = 0;
                Int32 intTimerAtHoursForFlightDefault = 7;
                Int32 intTimerAtMinForFlightDefault = 0;
                //objThreadStatusList = new List<ClsHoldThreadstate>();
                intTimerIntervalVal = Convert.ToInt32(ConfigurationManager.AppSettings["TimerInterval"].ToString());
                intTimerAtHours = Convert.ToInt32(ConfigurationManager.AppSettings["TimerAtHours"].ToString());
                intTimerAtMin = Convert.ToInt32(ConfigurationManager.AppSettings["TimerAtMin"].ToString());

                #region timer condition is for Cost and Price change 
                if (DateTime.Now.Hour == intTimerAtHours && DateTime.Now.Minute >= intTimerAtMin && DateTime.Now.Minute < intTimerAtMin + intTimerIntervalVal)
                {
                    // Log the Start of Process
                    LibraryUtil.EmailBody("Process started at: " + DateTime.Now);
                    BusinessProcess ObjbusinessProcess = new BusinessProcess();
                    //List<ClsHoldThreadstate> objThreadStatusList = new List<ClsHoldThreadstate>();
                    List<List<List<string>>> obj3DimensionList = ObjbusinessProcess.ProcessCSVWith3Dimension();
                    //List<List<List<string>>> obj3DimensionList = ProcessCSVWith3Dimension();
                    //DALAccess ObjDALAccess = new DALAccess();
                    //List<List<List<string>>> obj3DimensionList =  ObjDALAccess.ProcessCSVWith3Dimension();

                    Thread ThdForJobProcess1;
                    Int32 intnoOfLoop = obj3DimensionList.Count;

                    for (Int32 j = 0; j < intnoOfLoop; j++)
                    {
                        var driver = ObjbusinessProcess.LoginToApp(j.ToString());

                        ThdForJobProcess1 = new Thread(() => ObjbusinessProcess.ProcessCostPriceUpDate(driver, obj3DimensionList[j], j.ToString()));
                        ThdForJobProcess1.Start();
                        ThdForJobProcess1.Join(3000);// (3000);
                                                     //Thread.Sleep(3000);
                        ClsHoldThreadstate objClsHoldThreadstate = new ClsHoldThreadstate();
                        objClsHoldThreadstate.ThdList = ThdForJobProcess1.ManagedThreadId;
                        objClsHoldThreadstate.boolState = true;
                        objThreadStatusList.Add(objClsHoldThreadstate);
                        CostAndPriceUpdate.IntStatNoOfLoop++;
                    }
                }
                #endregion

                #region Below condition is for schedule time change

                intTimerAtHoursForScheduleChange = Convert.ToInt32(ConfigurationManager.AppSettings["TimerAtHoursForFlightChange"].ToString());
                intTimerAtMinForScheduleChange = Convert.ToInt32(ConfigurationManager.AppSettings["TimerAtMinForFlightChange"].ToString());
                if (DateTime.Now.Hour == intTimerAtHoursForScheduleChange && DateTime.Now.Minute >= intTimerAtMinForScheduleChange && DateTime.Now.Minute < intTimerAtMinForScheduleChange + intTimerIntervalVal)
                {
                    try
                    {
                        // Log the Start of Process
                        LibraryUtil.EmailBody("Flight Change Process started at:" + DateTime.Now);
                        FlightSchedureChange ObjFlightSchedureChange = new FlightSchedureChange();
                        //List<ClsHoldThreadstate> objThreadStatusList = new List<ClsHoldThreadstate>();
                        List<List<List<string>>> obj3DimensionList = ObjFlightSchedureChange.ProcessCSVWith3Dimension();
                        //List<List<List<string>>> obj3DimensionList = ProcessCSVWith3Dimension();

                        Thread ThdForJobProcess1;
                        Int32 intnoOfLoop = obj3DimensionList.Count;

                        for (Int32 j = 0; j < intnoOfLoop; j++)
                        {
                            var driver = ObjFlightSchedureChange.LoginToApp(j.ToString());

                            ThdForJobProcess1 = new Thread(() => ObjFlightSchedureChange.ProcessFlightTimeUpDate(driver, obj3DimensionList[j], j.ToString()));
                            ThdForJobProcess1.Start();
                            ThdForJobProcess1.Join(3000);// (3000);
                                                         //Thread.Sleep(3000);
                            ClsHoldThreadstate objClsHoldThreadstate = new ClsHoldThreadstate();
                            objClsHoldThreadstate.ThdList = ThdForJobProcess1.ManagedThreadId;
                            objClsHoldThreadstate.boolState = true;
                            objThreadStatusList.Add(objClsHoldThreadstate);
                            CostAndPriceUpdate.IntStatNoOfLoop++;
                            
                        }
                        


                    }
                    catch (Exception exc)
                    {
                        // Handle to Error Log
                        string StrErrorMessage = string.Empty;
                        string strStackTrace = string.Empty;
                        string strAreaOfError = string.Empty;

                        StrErrorMessage = exc.Message;
                        strStackTrace = exc.StackTrace.ToString();
                        strAreaOfError = exc.Source + " #region Below condition is for schedule time change";

                        LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

                    }
                }

                #endregion

                #region Below condition is for Default Flight Option

                intTimerAtHoursForFlightDefault = Convert.ToInt32(ConfigurationManager.AppSettings["TimerAtHoursForFlightDefault"].ToString());
                intTimerAtMinForFlightDefault = Convert.ToInt32(ConfigurationManager.AppSettings["TimerAtMinForFlightDefault"].ToString());

                if (DateTime.Now.Hour == intTimerAtHoursForFlightDefault && DateTime.Now.Minute >= intTimerAtMinForFlightDefault && DateTime.Now.Minute < intTimerAtMinForFlightDefault + intTimerIntervalVal)
                {
                    try
                    {
                        // Log the Start of Process
                        LibraryUtil.EmailBody("Flight Default Option Process started at:" + DateTime.Now);
                        ProcessFlightDefault ObjFlightDefaultOption = new ProcessFlightDefault();
                        DALAccess ObjDALAccess = new DALAccess();
                         List<List<FlightDefaultExt>> obj3DimensionList = ObjDALAccess.Get3DDataFromDefaultFlightViewExt();
                        
                        Thread ThdForJobProcess1;
                        Int32 intnoOfLoop = obj3DimensionList.Count;

                        for (Int32 j = 0; j < intnoOfLoop; j++)
                        {
                            var driver = ObjFlightDefaultOption.LoginToApp(j.ToString());

                            ThdForJobProcess1 = new Thread(() => ObjFlightDefaultOption.ProcessAllFlightDefaultExt(driver, obj3DimensionList[j], j.ToString()));
                            ThdForJobProcess1.Start();
                            ThdForJobProcess1.Join(3000);
                            ClsHoldThreadstate objClsHoldThreadstate = new ClsHoldThreadstate();
                            objClsHoldThreadstate.ThdList = ThdForJobProcess1.ManagedThreadId;
                            objClsHoldThreadstate.boolState = true;
                            objThreadStatusList.Add(objClsHoldThreadstate);
                            CostAndPriceUpdate.IntStatNoOfLoop++;

                        }



                    }
                    catch (Exception exc)
                    {
                        // Handle to Error Log
                        string StrErrorMessage = string.Empty;
                        string strStackTrace = string.Empty;
                        string strAreaOfError = string.Empty;

                        StrErrorMessage = exc.Message;
                        strStackTrace = exc.StackTrace.ToString();
                        strAreaOfError = exc.Source + " #region Below condition is for Default Flight Option";

                        LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

                    }
                }

                #endregion


            }
            catch (Exception exc)
            {  // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " private void Timer_Elapsed(object sender, ElapsedEventArgs e)";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }

}

        protected override void OnStop()
        {
        }

        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }
}
