﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using System.Configuration;
using System.IO;
using Renci.SshNet;
using OpenQA.Selenium.Remote;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System.Diagnostics;
using System.Management;

namespace CostAndPriceUpdate
{
    #region Class to change Flight Time
    class FlightSchedureChange
    {
        #region Login for givin Application
        public ChromeDriver LoginToApp(string strOrderOfLogin)
        {
            try
            {

                ChromeOptions options = new ChromeOptions();
                options.AcceptInsecureCertificates = true;
                options.AddArgument("--ignore-certificate-errors");
                options.AddArgument("--headless");
                options.AddArgument("--disable-gpu");
                options.AddArgument("--no-startup-window");
                options.UnhandledPromptBehavior = UnhandledPromptBehavior.Accept;

                //var chromeDriverService = ChromeDriverService.CreateDefaultService();
                var chromeDriverService = ChromeDriverService.CreateDefaultService(AppDomain.CurrentDomain.BaseDirectory);
                chromeDriverService.HideCommandPromptWindow = true;

               

                var increatetimeout = TimeSpan.FromMinutes(3);

                string strCheckStagingOrProd = ConfigurationManager.AppSettings["Staging"].ToString();
                string strLoginURL = string.Empty;
                if (strCheckStagingOrProd.ToLower().Contains("false"))
                {
                    strLoginURL = ConfigurationManager.AppSettings["TigerBayURLProduction"].ToString();
                }
                else
                {
                    strLoginURL = ConfigurationManager.AppSettings["TigerBayURLStaging"].ToString();
                }

                var driver = new ChromeDriver(chromeDriverService, options, increatetimeout);


                string strUserName = "vijay.kanaginhal@newmarketholidays.co.uk";
                string strPassword = "Welcome1!";



                driver.Manage().Window.Maximize();
                driver.Navigate().GoToUrl(@strLoginURL);

                var UserName = driver.FindElementById("Username");
                var Password = driver.FindElementById("Password");

                // ************** Live User Login
                //UserName.SendKeys("robot@newmarket.travel");
                //Password.SendKeys("Welcome1!");

                //***************** Staging User Login details
                //UserName.SendKeys("vijay.kanaginhal@newmarketholidays.co.uk");
                //Password.SendKeys("Welcome1!");

                // Get Username and password from web config, check for LIVE or prod from Config

               
                if (strCheckStagingOrProd.ToLower().Contains("false"))
                {
                    strUserName = ConfigurationManager.AppSettings["ProductionLogin"].ToString();
                    strPassword = ConfigurationManager.AppSettings["ProductionPassword"].ToString();
                }

                UserName.SendKeys(strUserName);
                Password.SendKeys(strPassword);


                var Login = driver.FindElementByName("button");

                Login.Click();
                LibraryUtil.LogActivitiesForChangeTime(": Start of Login: for Process :" + strOrderOfLogin.ToString() + "@:" + DateTime.Now, strOrderOfLogin);
                var SessionForTiger = driver.SessionId;

                return driver;

            }
            catch (Exception exc)
            {
                // Handle to Error Log

                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " Flight time Change Application Login Page";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
                return null;

            }


        }
        #endregion

        
        #region Process Schedule Flight timing update with Given 3 Dimension generic List,with Unique Log No
        public Boolean ProcessFlightTimeUpDate(ChromeDriver driver, List<List<string>> objSlicedList, string LogFileExtn)
        {
            Boolean BoolRetVal = false;


            try
            {


                

                //string strSearchURL = @"https://newmarket-production.ontigerbay.co.uk:443/admin/stock/flights/ScheduleChanges/fSearch.aspx";

                string StrFlightNumner = "cph_main_txtSearchFlightNumber";
                string strFlightTimeCheckBx = "cph_main_txtBookingDate_chkSelect";
                string StrSearchURL = string.Empty;
                StrSearchURL = ConfigurationManager.AppSettings["strSearchURL"].ToString();

                string strFromDateControl =  "cph_main_txtBookingDate_txt_date1";
                string strToDateControl =   "cph_main_txtBookingDate_txt_date2";

                string strSearchButton = "cph_main_btnSearch";
                List<List<string>> TotalList = new List<List<string>>();
                TotalList = objSlicedList;
                //int i = 0;
                //"On: " + DateTime.Now.ToString() + 
                //LibraryUtil.LogActivities(DateTime.Now + "No : " + i.ToString());
                var objLock = new Object();

                //foreach (List<string> LstEachLineVal in TotalList)
                //{

                Int32 intExactSlicedArry = 0;
                intExactSlicedArry = Convert.ToInt32(LogFileExtn);
                int i = 0;

                //foreach (string EachLineVal in LstEachLineVal)
                //foreach (string EachLineVal in TotalList[intExactSlicedArry])
                //for (int NoOfRec = 0; NoOfRec < TotalList[intExactSlicedArry].Count; NoOfRec++)
                for (int NoOfRec = 0; NoOfRec < TotalList[0].Count; NoOfRec++)
                {
                    lock (Thread.CurrentThread)
                    {
                        LibraryUtil.LogActivitiesForChangeTime(DateTime.Now + "No :( " + i.ToString() + ") ", LogFileExtn);
                        LibraryUtil.LogActivitiesForChangeTime("---------------------------------", LogFileExtn);
                        //foreach (string strGoHyper in EachLineVal)
                        //{
                        if (StrSearchURL.Contains("http"))
                        {
                            DateTime DtDepartureDate = new DateTime();
                            if (DateTime.TryParse(TotalList[1][i], out DtDepartureDate))
                            {


                                try
                                {

                                    StrSearchURL = ChangeStagingURL(StrSearchURL);
                                    //LibraryUtil.LogActivities("PriceWebLink: " + strPriceURL.ToString(), LogFileExtn);
                                    driver.Navigate().GoToUrl(StrSearchURL);
                                    var FlightNumber = TotalList[0][i].ToString();


                                    //Check Box Control
                                    var strFlightTimeCheckBxId = driver.FindElementById(strFlightTimeCheckBx);
                                    strFlightTimeCheckBxId.Click();//.SendKeys("true");

                                    //From Date Box Control
                                    var FromDate = TotalList[1][i].ToString();
                                    var strFlightFromDateId = driver.FindElementById(strFromDateControl);
                                    strFlightFromDateId.Clear();
                                    strFlightFromDateId.SendKeys(FromDate);


                                    //To Date Box Control
                                    var ToDate = TotalList[1][i].ToString();
                                    var strToDateControlId = driver.FindElementById(strToDateControl);
                                    strToDateControlId.Clear();
                                    strToDateControlId.SendKeys(FromDate);

                                    var FlightNumberControlId = driver.FindElementById(StrFlightNumner);

                                    IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
                                    jse.ExecuteScript(StrFlightNumner, FlightNumberControlId);
                                    FlightNumberControlId.Clear();
                                    FlightNumberControlId.SendKeys(FlightNumber);


                                    var VarSearch = driver.FindElementById(strSearchButton);


                                    jse.ExecuteScript("arguments[0].click()", VarSearch);

                                    LibraryUtil.LogActivitiesForChangeTime("Flight Number : " + FlightNumber, LogFileExtn);
                                    LibraryUtil.LogActivitiesForChangeTime("From Date : " + FromDate, LogFileExtn);
                                    LibraryUtil.LogActivitiesForChangeTime("To Date : " + FromDate, LogFileExtn);
                                    LibraryUtil.LogActivitiesForChangeTime("Search Successfully", LogFileExtn);


                                    #region Proceed part to moveahead
                                    string strProceedBtn = "cph_main_btnBeginUpdate";
                                    var VarProceed = driver.FindElementById(strProceedBtn);


                                    jse.ExecuteScript("arguments[0].click()", VarProceed);
                                    LibraryUtil.LogActivitiesForChangeTime("Proceed Clicked : ", LogFileExtn);

                                    #endregion

                                    #region Check time for selected Flight

                                    string strDepartureTimeChkBx = "cph_main_chkDepartureTime";
                                    string strDepartureTimeInputTxt = "cph_main_ucStartTime_txt_time";
                                    string strArrivalTimeChkBx = "cph_main_chkArrivalTime";
                                    string strArrivalTimeInputTxt = "cph_main_ucEndTime_txt_time";
                                    string strCheckInTimeChkBx = "cph_main_chkCheckInTime";
                                    string strCheckInTimeInputTxt = "cph_main_ucCheckInTime_txt_time";

                                    // Batch Flight change Document if checked, will send notification to Customer. Heance for testing please uncheck.
                                    string strBatchDocumentFltChange = "cph_main_chkSendToBatch";


                                    string strNewDepartureTime = TotalList[6][i].ToString();
                                    string strNewArrivalTime = TotalList[7][i].ToString();
                                    string strNewCheckInTime = TotalList[8][i].ToString();

                                    // New Departure Time
                                    var varDepartureTimeChkBx = driver.FindElementById(strDepartureTimeChkBx);
                                    varDepartureTimeChkBx.Click();
                                    var varNewDepartureTime = driver.FindElementById(strDepartureTimeInputTxt);
                                    varNewDepartureTime.Clear();
                                    varNewDepartureTime.SendKeys(strNewDepartureTime);

                                    LibraryUtil.LogActivitiesForChangeTime("Departure Time @" + strNewDepartureTime + " : entered : ", LogFileExtn);

                                    // New Arrival Time
                                    var varArrivalTimeChkBx = driver.FindElementById(strArrivalTimeChkBx);
                                    varArrivalTimeChkBx.Click();
                                    var varNewArrivalTime = driver.FindElementById(strArrivalTimeInputTxt);
                                    varNewArrivalTime.Clear();
                                    varNewArrivalTime.SendKeys(strNewArrivalTime);

                                    LibraryUtil.LogActivitiesForChangeTime("Arrival Time @" + strNewArrivalTime + " : entered : ", LogFileExtn);

                                    // New Check in Time
                                    var varNewCheckInTimeChkBx = driver.FindElementById(strCheckInTimeChkBx);
                                    varNewCheckInTimeChkBx.Click();
                                    var varNewCheckInTime = driver.FindElementById(strCheckInTimeInputTxt);
                                    varNewCheckInTime.Clear();
                                    varNewCheckInTime.SendKeys(strNewCheckInTime);

                                    LibraryUtil.LogActivitiesForChangeTime("Check in Time @" + strNewCheckInTime + " : entered : ", LogFileExtn);

                                    // below code is for Email Information for click. Fr testingplease uncheckthis ehck Box
                                    var varEmailNotifyToCustomerChkBx = driver.FindElementById(strBatchDocumentFltChange);

                                    string strCheckStagingForBatchEmail = ConfigurationManager.AppSettings["BatchEmailToProcess"].ToString();
                                    string strLoginURL = string.Empty;
                                    if (strCheckStagingForBatchEmail.ToLower().Contains("true"))
                                    {
                                        LibraryUtil.LogActivitiesForChangeTime("Email Notification Status @ " + TotalList[9][i].ToString(), LogFileExtn);
                                        // Check for Email Alert approval from client
                                        if (TotalList[9][i].ToString().ToLower().Equals("yes"))
                                        {
                                            varEmailNotifyToCustomerChkBx.Click();
                                            LibraryUtil.LogActivitiesForChangeTime("Email Notification Clicked @ ", LogFileExtn);
                                            
                                        }
                                    }

                                    // update time
                                    string strTimeChangeUpdateBtn = "cph_main_btnSubmitUpdate";
                                    var VarUpdate = driver.FindElementById(strTimeChangeUpdateBtn);
                                    jse.ExecuteScript("arguments[0].click()", VarUpdate);

                                    LibraryUtil.LogActivitiesForChangeTime("Submited time change @ ", LogFileExtn);

                                    #endregion



                                }
                                catch (Exception exc)
                                {
                                    // Handle to Error Log
                                    string StrErrorMessage = string.Empty;
                                    string strStackTrace = string.Empty;
                                    string strAreaOfError = string.Empty;

                                    StrErrorMessage = "Process No: (" + LogFileExtn + ")" + exc.Message + Environment.NewLine;
                                    StrErrorMessage = StrErrorMessage + "ProblemURL: " + ChangeStagingURL(TotalList[0][i].ToString()) + Environment.NewLine;
                                    StrErrorMessage = StrErrorMessage + "Problem Price : " + TotalList[6][i].ToString() + Environment.NewLine;
                                    strStackTrace = exc.StackTrace.ToString();
                                    strAreaOfError = exc.Source + " Occured in Web HyperLink URL Crome Drive";

                                    LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
                                    LibraryUtil.LogActivitiesForChangeTime("Schedule Time change Problem", LogFileExtn);

                                    // handle timed out Exception
                                    if (StrErrorMessage.ToLower().Contains("timed out"))
                                    {
                                        driver = LoginToApp(LogFileExtn);
                                        i--;
                                    }
                                    // handle Alert Exception 
                                    if (StrErrorMessage.ToLower().Contains("unexpected alert"))
                                    {
                                        //driver.SwitchTo().Alert().Accept();
                                        //driver = LoginToApp(LogFileExtn);
                                        //i--;
                                    }
                                }
                            }
                            // ********************************
                            
                        }


                    }
                    i++;

                   

                }

                //}

                driver.Close();
                
                LibraryUtil.LogActivitiesForChangeTime("End Of Process: with following Thread " + LogFileExtn + "@: " + DateTime.Now +" @ : ", LogFileExtn);
                //foreach (ClsHoldThreadstate objClsHoldThreadstate in Form1.objThreadStatusList)
                //{
                //    if (Thread.CurrentThread.ManagedThreadId == objClsHoldThreadstate.ThdList)
                //    {
                //        objClsHoldThreadstate.boolState = false;
                //    }
                //}

                foreach (ClsHoldThreadstate objClsHoldThreadstate in CostAndPriceUpdate.objThreadStatusList)
                {
                    if (Thread.CurrentThread.ManagedThreadId == objClsHoldThreadstate.ThdList)
                    {
                        objClsHoldThreadstate.boolState = false;
                    }
                }
                driver.Quit();
                CostAndPriceUpdate.IntStatNoOfLoop--;
                driver.Dispose();
                // for mail generation

                CheckforEmailSend();
            }
            catch (Exception exc)
            {

                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " ProcessCostPriceUpDate(driver, obj3DimensionList[j])";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

                // report thisvia email

            }

            return BoolRetVal;

        }
        #endregion


        #region process CSV file and slice to process in multithread
        public List<List<List<string>>> ProcessCSVWith3Dimension()
        {
            List<List<List<string>>> ObjMultipleList = new List<List<List<string>>>();
            try
            {
                string strCSVFilePathWithFile = string.Empty;

                FTPProcess objFTPProcess = new FTPProcess();

                //strCSVFilePathWithFile = ConfigurationManager.AppSettings["CSVFilePath"].ToString();

                strCSVFilePathWithFile = objFTPProcess.ProcessFTPDownload("FlightTimingUpdate");
                List<List<string>> TotalList = new List<List<string>>();

                 
                TotalList =  ProcessCSVFiles(strCSVFilePathWithFile);

                // Now based on count of records plan create multiple threads

                Int32 intNoOfRecords = TotalList[0].Count;




                Int32 IntNoOfRecordProcess = 0;
                Int32 IntNoOfthreads = 0;
                //IntNoOfRecordProcess = Convert.ToInt32(ConfigurationManager.AppSettings["NoOfRecordToProcess"].ToString());
                IntNoOfthreads = Convert.ToInt32(ConfigurationManager.AppSettings["NoOfThread"].ToString());
                if (TotalList[0].Count > 50)
                {
                    IntNoOfRecordProcess = TotalList[0].Count / IntNoOfthreads + 1;
                }
                //else
                //{

                //}

                //if (ObjMultipleList[0] TotalList[0].Count>20)

                if (IntNoOfRecordProcess > 0)
                {
                    do
                    {
                        if ((ObjMultipleList.Count > 0) && (TotalList[0].Count < IntNoOfRecordProcess))
                        {
                            IntNoOfRecordProcess = TotalList[0].Count;
                        }
                        List<List<string>> TotalList1 = new List<List<string>>();

                        List<string> InnerListstp0 = new List<string>();


                        InnerListstp0.AddRange(TotalList[0].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp0);
                        TotalList[0].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp1 = new List<string>();
                        InnerListstp1.AddRange(TotalList[1].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp1);
                        TotalList[1].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp2 = new List<string>();
                        InnerListstp2.AddRange(TotalList[2].Take(IntNoOfRecordProcess).ToArray());//.ToArray()
                        TotalList1.Add(InnerListstp2);
                        TotalList[2].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp3 = new List<string>();
                        InnerListstp3.AddRange(TotalList[3].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp3);
                        TotalList[3].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp4 = new List<string>();
                        InnerListstp4.AddRange(TotalList[4].Take(IntNoOfRecordProcess).ToArray());//.ToArray()
                        TotalList1.Add(InnerListstp4);
                        TotalList[4].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp5 = new List<string>();
                        InnerListstp5.AddRange(TotalList[5].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp5);
                        TotalList[5].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp6 = new List<string>();
                        InnerListstp6.AddRange(TotalList[6].Take(IntNoOfRecordProcess).ToArray());//.ToArray()
                        TotalList1.Add(InnerListstp6);
                        TotalList[6].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp7 = new List<string>();
                        InnerListstp7.AddRange(TotalList[7].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp7);
                        TotalList[7].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp8 = new List<string>();
                        InnerListstp8.AddRange(TotalList[8].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp8);
                        TotalList[8].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp9 = new List<string>();
                        InnerListstp9.AddRange(TotalList[9].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp9);
                        TotalList[9].RemoveRange(0, IntNoOfRecordProcess);




                        ObjMultipleList.Add(TotalList1);

                    } while (!(ObjMultipleList[0][0].Count - TotalList[0].Count > IntNoOfRecordProcess));
                }
                else
                {
                    List<List<string>> TotalList1 = new List<List<string>>();

                    List<string> InnerListstp0 = new List<string>();


                    InnerListstp0.AddRange(TotalList[0].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp0);
                    TotalList[0].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp1 = new List<string>();
                    InnerListstp1.AddRange(TotalList[1].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp1);
                    TotalList[1].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp2 = new List<string>();
                    InnerListstp2.AddRange(TotalList[2].ToArray());//.ToArray()
                    TotalList1.Add(InnerListstp2);
                    TotalList[2].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp3 = new List<string>();
                    InnerListstp3.AddRange(TotalList[3].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp3);
                    TotalList[3].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp4 = new List<string>();
                    InnerListstp4.AddRange(TotalList[4].ToArray());//.ToArray()
                    TotalList1.Add(InnerListstp4);
                    TotalList[4].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp5 = new List<string>();
                    InnerListstp5.AddRange(TotalList[5].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp5);
                    TotalList[5].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp6 = new List<string>();
                    InnerListstp6.AddRange(TotalList[6].ToArray());//.ToArray()
                    TotalList1.Add(InnerListstp6);
                    TotalList[6].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp7 = new List<string>();
                    InnerListstp7.AddRange(TotalList[7].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp7);
                    TotalList[7].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp8 = new List<string>();
                    InnerListstp8.AddRange(TotalList[8].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp8);
                    TotalList[8].RemoveRange(0, IntNoOfRecordProcess);

                    List<string> InnerListstp9 = new List<string>();
                    InnerListstp9.AddRange(TotalList[9].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp9);
                    TotalList[9].RemoveRange(0, IntNoOfRecordProcess);

                    ObjMultipleList.Add(TotalList1);
                }




            }
            catch (Exception exc)
            {  // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public List<List<List<string>>> ProcessCSVWith3Dimension()in Flight Time Change";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }

            return ObjMultipleList;
        }
        #endregion

        #region check for email to be procesed
        public void CheckforEmailSend()
        {
            Boolean SendStatus = true;

            foreach (ClsHoldThreadstate objClsHoldThreadstate in CostAndPriceUpdate.objThreadStatusList)
            {
                if (objClsHoldThreadstate.boolState)
                {
                    SendStatus = false;
                    break;
                }

            }

            //if ((SendStatus) && (CostAndPriceUpdate.IntStatNoOfLoop == 0))
            if ( CostAndPriceUpdate.IntStatNoOfLoop == 0)
            {
                // Log the Start of Process
                CostAndPriceUpdate.objThreadStatusList.Clear();
                CostAndPriceUpdate.IntStatNoOfLoop = 0;
                LibraryUtil.EmailBody("Process finished at: " + DateTime.Now);
                // call Static Email Method
                LibraryUtil.SendLogEmail("l", "time");
                LibraryUtil.SendLogEmail("e", "time");
                LibraryUtil.SendEmailBodyWithStatus("time");
            }
            
        }
        #endregion

        #region This metthod is called only when testing dummy data ( Staging server)
        public string ChangeStagingURL(string strPriceURL)
        {
            string strRetURL = strPriceURL;

            try
            {
                string CheckStaging = ConfigurationManager.AppSettings["Staging"].ToString();
                if (CheckStaging.ToLower().Contains("true"))
                {
                    strRetURL = strPriceURL.Replace("production", "staging");
                }
            }
            catch (Exception exc)
            {  // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public string ChangeStagingURL(string strPriceURL)";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }
            return strRetURL;

        }
        #endregion



        #region Process CSV Files
        public List<List<string>> ProcessCSVFiles(string strCSVFilePath)
        {
            List<List<string>> TotalList = new List<List<string>>();

            try
            {
                using (var reader = new StreamReader(@strCSVFilePath))
                {
                                    //CurrentFlightNumber
                                    //CurrentDepartureDate
                                    //CurrentAirline
                                    //CurrentOperatingCarrier
                                    //NewAirline
                                    //NewOperatingCarrier
                                    //NewDepartureTime
                                    //NewArrivalTime
                                    //NewCheckInTime
                                    //BatchFlightChangeDocuments

                    List<string> listA = new List<string>();
                    List<string> listB = new List<string>();

                    List<string> CurrentFlightNumber = new List<string>();
                    List<string> CurrentDepartureDate = new List<string>();
                    List<string> CurrentAirline = new List<string>();
                    List<string> CurrentOperatingCarrier = new List<string>();
                    List<string> NewAirline = new List<string>();
                    List<string> NewOperatingCarrier = new List<string>();
                    List<string> NewDepartureTime = new List<string>();
                    List<string> NewArrivalTime = new List<string>();
                    List<string> NewCheckInTime = new List<string>();
                    
                    List<string> BatchFlightChangeDocuments = new List<string>(); //This will send Email to Customer about flight change

                    #region column No for respective row to be using 
                    //TBCostWeblink       48
                    //TBPriceWeblink      49
                    //RowID       53
                    //AdultCost       42
                    //AdultPrice      47
                    //UpdatedCost     60
                    //UpdatedPrice        61
                    //OutboundFlightDate      58
                    //OuboundFlightNumber     59
                    #endregion

                    char[] CommaSap = new char[3];
                    CommaSap[0] = ' ';
                    CommaSap[1] = ',';
                    CommaSap[2] = ' ';


                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        line = CheckFieldEnclosed(line);
                        var values = line.Split(',');

                        if (values.Length > 0)
                        {

                            for (int i = 0; i < values.Length; i++)
                            {
                                //switch (values[i].ToLower())
                                switch (i)
                                {
                                    //case "tbcostweblink":
                                    case 0:
                                        CurrentFlightNumber.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "tbpriceweblink":
                                    case 1:
                                        CurrentDepartureDate.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "rowid":
                                    case 2:
                                        CurrentAirline.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "adultcost":
                                    case 3:
                                        CurrentOperatingCarrier.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "adultprice":
                                    case 4:
                                        NewAirline.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "updatedcost":
                                    case 5:
                                        NewOperatingCarrier.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "updatedprice":
                                    case 6:
                                        NewDepartureTime.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "outboundflightdate":
                                    case 7:
                                        NewArrivalTime.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "ouboundflightnumber":
                                    case 8:
                                        NewCheckInTime.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    case 9:
                                        BatchFlightChangeDocuments.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                }




                            }





                        }
                    }
                     

                    TotalList.Add(CurrentFlightNumber);
                    TotalList.Add(CurrentDepartureDate);
                    TotalList.Add(CurrentAirline);
                    TotalList.Add(CurrentOperatingCarrier);
                    TotalList.Add(NewAirline);
                    TotalList.Add(NewOperatingCarrier);
                    TotalList.Add(NewDepartureTime);
                    TotalList.Add(NewArrivalTime);
                    TotalList.Add(NewCheckInTime);
                    TotalList.Add(BatchFlightChangeDocuments);
                }
            }
            catch (Exception exc)
            {
                //handle Exceptionhere
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " Occured in Process CSV File Mathod";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }
            Int32 intNoOfrecords = TotalList[0].Count;
            // Log the Start of Process
            LibraryUtil.EmailBody("No of Records :" + intNoOfrecords.ToString());
            return TotalList;

        }
        #endregion

        #region Below Method will check for comma in Quotes and replace it with Pipe
        public string CheckFieldEnclosed(string strInput)
        {
            // Check Double Quote and replace, to Pipe
            string strGetStringToReplace = strInput;
            string strReplceString = strInput;
            int intStartInd = 0;
            int intEndIndex = 0;
            int intStartPoint = 0;
            try
            {
                do
                {
                    intStartInd = strInput.IndexOf("\"", intStartPoint);
                    if (intStartInd >= 0)
                    {
                        intEndIndex = strInput.IndexOf("\"", intStartInd + 1);
                        strGetStringToReplace = strInput.Substring(intStartInd, intEndIndex - intStartInd);
                        strInput = strInput.Replace(strGetStringToReplace, strGetStringToReplace.Replace(',', '|'));
                        intStartPoint = intEndIndex + 1;
                    }
                } while (strInput.IndexOf("\"", intStartPoint) > 0);

            }
            catch (Exception exc)
            {
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " Occured in CheckFieldEnclosed(string strInput)";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }
            return strInput;

        }
        #endregion

        #region replace Pipe with Comma
        public string CheckPipeAndRemove(string strCheckPipeAndRemove)
        {
            string strReturn = strCheckPipeAndRemove.Replace('|', ',');

            return strReturn;

        }
        #endregion

        #region replace Pipe with Comma
        public string CheckPipeDoubleQuoteAndRemove(string strCheckPipeAndRemove)
        {
            string strReturn = strCheckPipeAndRemove.Replace('|', ',');
            strReturn = strCheckPipeAndRemove.Replace("\"", "");
            return strReturn;

        }
        #endregion

    }
    #endregion


}
