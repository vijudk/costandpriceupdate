﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace CostAndPriceUpdate
{
    class DALAccess
    {
        SqlConnection Con;
        #region process CSV file and slice to process in multithread
        public List<List<List<string>>> ProcessCSVWith3Dimension()
        {
            List<List<List<string>>> ObjMultipleList = new List<List<List<string>>>();
            try
            {
                string strCSVFilePathWithFile = string.Empty;

               
                List<List<string>> TotalList = new List<List<string>>();

                BusinessProcess objBusinessProcess = new BusinessProcess();

                //TotalList = objBusinessProcess.ProcessCSVFiles(strCSVFilePathWithFile);
                TotalList = GetDataFromView();
                
                // Now based on count of records plan create multiple threads

                Int32 intNoOfRecords = TotalList[0].Count;




                Int32 IntNoOfRecordProcess = 0;
                Int32 IntNoOfthreads = 0;
                //IntNoOfRecordProcess = Convert.ToInt32(ConfigurationManager.AppSettings["NoOfRecordToProcess"].ToString());
                IntNoOfthreads = Convert.ToInt32(ConfigurationManager.AppSettings["NoOfThread"].ToString());
                if (TotalList[0].Count > 50)
                {
                    IntNoOfRecordProcess = TotalList[0].Count / IntNoOfthreads + 1;
                }
                //else
                //{

                //}

                //if (ObjMultipleList[0] TotalList[0].Count>20)

                if (IntNoOfRecordProcess > 0)
                {
                    do
                    {
                        if ((ObjMultipleList.Count > 0) && (TotalList[0].Count < IntNoOfRecordProcess))
                        {
                            IntNoOfRecordProcess = TotalList[0].Count;
                        }
                        List<List<string>> TotalList1 = new List<List<string>>();

                        List<string> InnerListstp0 = new List<string>();


                        InnerListstp0.AddRange(TotalList[0].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp0);
                        TotalList[0].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp1 = new List<string>();
                        InnerListstp1.AddRange(TotalList[1].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp1);
                        TotalList[1].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp2 = new List<string>();
                        InnerListstp2.AddRange(TotalList[2].Take(IntNoOfRecordProcess).ToArray());//.ToArray()
                        TotalList1.Add(InnerListstp2);
                        TotalList[2].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp3 = new List<string>();
                        InnerListstp3.AddRange(TotalList[3].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp3);
                        TotalList[3].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp4 = new List<string>();
                        InnerListstp4.AddRange(TotalList[4].Take(IntNoOfRecordProcess).ToArray());//.ToArray()
                        TotalList1.Add(InnerListstp4);
                        TotalList[4].RemoveRange(0, IntNoOfRecordProcess);
                        //List<string> InnerListstp5 = new List<string>();
                        //InnerListstp5.AddRange(TotalList[5].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        //TotalList1.Add(InnerListstp5);
                        //TotalList[5].RemoveRange(0, IntNoOfRecordProcess);
                        //List<string> InnerListstp6 = new List<string>();
                        //InnerListstp6.AddRange(TotalList[6].Take(IntNoOfRecordProcess).ToArray());//.ToArray()
                        //TotalList1.Add(InnerListstp6);
                        //TotalList[6].RemoveRange(0, IntNoOfRecordProcess);
                        //List<string> InnerListstp7 = new List<string>();
                        //InnerListstp7.AddRange(TotalList[7].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        //TotalList1.Add(InnerListstp7);
                        //TotalList[7].RemoveRange(0, IntNoOfRecordProcess);
                        //List<string> InnerListstp8 = new List<string>();
                        //InnerListstp8.AddRange(TotalList[8].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        //TotalList1.Add(InnerListstp8);
                        //TotalList[8].RemoveRange(0, IntNoOfRecordProcess);




                        ObjMultipleList.Add(TotalList1);

                    } while (!(ObjMultipleList[0][0].Count - TotalList[0].Count > IntNoOfRecordProcess));
                }
                else
                {
                    List<List<string>> TotalList1 = new List<List<string>>();

                    List<string> InnerListstp0 = new List<string>();


                    InnerListstp0.AddRange(TotalList[0].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp0);
                    TotalList[0].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp1 = new List<string>();
                    InnerListstp1.AddRange(TotalList[1].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp1);
                    TotalList[1].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp2 = new List<string>();
                    InnerListstp2.AddRange(TotalList[2].ToArray());//.ToArray()
                    TotalList1.Add(InnerListstp2);
                    TotalList[2].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp3 = new List<string>();
                    InnerListstp3.AddRange(TotalList[3].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp3);
                    TotalList[3].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp4 = new List<string>();
                    InnerListstp4.AddRange(TotalList[4].ToArray());//.ToArray()
                    TotalList1.Add(InnerListstp4);
                    TotalList[4].RemoveRange(0, IntNoOfRecordProcess);
                    //List<string> InnerListstp5 = new List<string>();
                    //InnerListstp5.AddRange(TotalList[5].ToArray());//.ToArray();
                    //TotalList1.Add(InnerListstp5);
                    //TotalList[5].RemoveRange(0, IntNoOfRecordProcess);
                    //List<string> InnerListstp6 = new List<string>();
                    //InnerListstp6.AddRange(TotalList[6].ToArray());//.ToArray()
                    //TotalList1.Add(InnerListstp6);
                    //TotalList[6].RemoveRange(0, IntNoOfRecordProcess);
                    //List<string> InnerListstp7 = new List<string>();
                    //InnerListstp7.AddRange(TotalList[7].ToArray());//.ToArray();
                    //TotalList1.Add(InnerListstp7);
                    //TotalList[7].RemoveRange(0, IntNoOfRecordProcess);
                    //List<string> InnerListstp8 = new List<string>();
                    //InnerListstp8.AddRange(TotalList[8].ToArray());//.ToArray();
                    //TotalList1.Add(InnerListstp8);
                    //TotalList[8].RemoveRange(0, IntNoOfRecordProcess);

                    ObjMultipleList.Add(TotalList1);
                }




            }
            catch (Exception exc)
            {  // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public List<List<List<string>>> ProcessCSVWith3Dimension()";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }

            return ObjMultipleList;
        }
        #endregion

        #region Process CSV Files
        public List<List<string>> GetDataFromView()
        {
            List<List<string>> TotalList = new List<List<string>>();
            string strConnString = ConfigurationManager.ConnectionStrings["SQLConnForDB3"].ToString();
            Con = new SqlConnection(strConnString);
            if(Con.State!= System.Data.ConnectionState.Closed)
            Con.Open();

            try
            {


                    List<string> TBPriceWeblink = new List<string>();
                    List<string> TBCostWeblink = new List<string>();
                    List<string> RowID = new List<string>();
                    List<string> AdultCost = new List<string>();
                    List<string> AdultPrice = new List<string>();
                    List<string> UpdatedCost = new List<string>();
                    List<string> UpdatedPrice = new List<string>();
                    List<string> OutboundFlightDate = new List<string>();
                    List<string> OuboundFlightNumber = new List<string>();

                #region column No for respective row to be using 
                //TBCostWeblink       48
                //TBPriceWeblink      49
                //RowID       53
                //AdultCost       42
                //AdultPrice      47
                //UpdatedCost     60
                //UpdatedPrice        61
                //OutboundFlightDate      58
                //OuboundFlightNumber     59
                #endregion 

                string strQueryForPriceAndCost = "Select TBPriceWeblink,TBCostWeblink,RowID,AdultCost,AdultPrice,UpdatedCost,UpdatedPrice,OutboundFlightDate,OuboundFlightNumber  from ext.vFlightCostPrices";

                using (SqlCommand objSqlCommandForUpdatePrice = new SqlCommand(strQueryForPriceAndCost, Con))
                {

                    objSqlCommandForUpdatePrice.CommandType = System.Data.CommandType.Text;
                    objSqlCommandForUpdatePrice.CommandText = strQueryForPriceAndCost;
                    objSqlCommandForUpdatePrice.CommandTimeout = 300;
                    SqlDataReader objSQLDataReader;

                    objSQLDataReader = objSqlCommandForUpdatePrice.ExecuteReader();

                    if (objSQLDataReader.HasRows)
                    {
                        while (objSQLDataReader.Read())
                        {

                            TBPriceWeblink.Add(objSQLDataReader["TBPriceWeblink"].ToString());
                            TBCostWeblink.Add(objSQLDataReader["TBCostWeblink"].ToString());
                            RowID.Add(objSQLDataReader["RowID"].ToString());
                            AdultCost.Add(objSQLDataReader["AdultCost"].ToString());
                            AdultPrice.Add(objSQLDataReader["AdultPrice"].ToString());
                            UpdatedCost.Add(objSQLDataReader["UpdatedCost"].ToString());
                            UpdatedPrice.Add(objSQLDataReader["UpdatedPrice"].ToString());
                            OutboundFlightDate.Add(objSQLDataReader["OutboundFlightDate"].ToString());
                            OuboundFlightNumber.Add(objSQLDataReader["OuboundFlightNumber"].ToString());
                        }
                    }
                }


         

                    TotalList.Add(TBPriceWeblink);
                    TotalList.Add(TBCostWeblink);
                    TotalList.Add(RowID);
                    TotalList.Add(AdultCost);
                    TotalList.Add(AdultPrice);
                    TotalList.Add(UpdatedCost);
                    TotalList.Add(UpdatedPrice);
                    TotalList.Add(OutboundFlightDate);
                    TotalList.Add(OuboundFlightNumber);

                
            }
            catch (Exception exc)
            {
                //handle Exceptionhere
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public List<List<string>> GetDataFromView()";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }
            Int32 intNoOfrecords = TotalList[0].Count;
            // Log the Start of Process
            LibraryUtil.EmailBody("No of Records :" + intNoOfrecords.ToString());
            return TotalList;

        }
        #endregion


        #region Process Flight Default from View
        public List<FlightDefault> GetDataFromDefaultFlightView()
        {
            List<FlightDefault> objLstflightDefaults = new List<FlightDefault>();
            string strConnString = ConfigurationManager.ConnectionStrings["SQLConnForDB3"].ToString();
            SqlConnection Con = new SqlConnection(strConnString);
            Con.Open();

            try
            {





                #region column No for respective row to be using 
                //public string strFlightNo { get; set; }
                //public string strURLForChangeDefault { get; set; }
                //public string strDropDownType { get; set; }
                //public string strRowId { get; set; }
                //  [Flightnumber]
                //,[TBPriceWeblink]
                //,[RateOption]
                #endregion

                string strQueryForPriceAndCost = "Select * from ext.vFlightDefaults";
                SqlCommand objSqlCommandForUpdatePrice = new SqlCommand(strQueryForPriceAndCost, Con);
                objSqlCommandForUpdatePrice.CommandType = System.Data.CommandType.Text;
                objSqlCommandForUpdatePrice.CommandText = strQueryForPriceAndCost;
                objSqlCommandForUpdatePrice.CommandTimeout = 360;
                SqlDataReader objSQLDataReader;

                objSQLDataReader = objSqlCommandForUpdatePrice.ExecuteReader();

                if (objSQLDataReader.HasRows)
                {
                    while (objSQLDataReader.Read())
                    {
                        FlightDefault objTempFlightDefault = new FlightDefault();
                        //objLstflightDefaults

                        objTempFlightDefault.strFlightNo = objSQLDataReader[0].ToString();
                        objTempFlightDefault.strURLForChangeDefault = objSQLDataReader[1].ToString();
                        objTempFlightDefault.strDropDownType = objSQLDataReader[2].ToString();

                        objLstflightDefaults.Add(objTempFlightDefault);
                    }
                }







            }
            catch (Exception exc)
            {
                //handle Exceptionhere
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public List<FlightDefault> GetDataFromDefaultFlightView()";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }

            Int32 intNoOfrecords = objLstflightDefaults.Count;
            // Log the Start of Process
            LibraryUtil.EmailBody("No of Records :" + intNoOfrecords.ToString());
            return objLstflightDefaults;

        }
        #endregion

        #region Ger 3D object flightdefault data
        public List<List<FlightDefault>> Get3DDataFromDefaultFlightView()
        {
            List<List<FlightDefault>> objListListFlightDefault = new List<List<FlightDefault>>();
            try
            {
                List<FlightDefault> objLstFlightDefault = GetDataFromDefaultFlightView();
                Int32 IntNoOfRecordProcess = 0;
                Int32 IntNoOfthreads = 0;
                //IntNoOfRecordProcess = Convert.ToInt32(ConfigurationManager.AppSettings["NoOfRecordToProcess"].ToString());
                IntNoOfthreads = Convert.ToInt32(ConfigurationManager.AppSettings["NoOfThread"].ToString());
                if (objLstFlightDefault.Count > 50)
                {
                    IntNoOfRecordProcess = objLstFlightDefault.Count / IntNoOfthreads + 1;
                }


                if (IntNoOfRecordProcess > 0)
                {
                    do
                    {
                        if ((objListListFlightDefault.Count > 0) && (objLstFlightDefault.Count < IntNoOfRecordProcess))
                        {
                            IntNoOfRecordProcess = objLstFlightDefault.Count;
                        }
                        List<FlightDefault> TotalList1 = new List<FlightDefault>();

                        List<FlightDefault> InnerListstp0 = new List<FlightDefault>();


                        InnerListstp0.AddRange(objLstFlightDefault.Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.AddRange(InnerListstp0);
                        objLstFlightDefault.RemoveRange(0, IntNoOfRecordProcess);






                        objListListFlightDefault.Add(TotalList1);

                    } while (!(objListListFlightDefault[0].Count - objLstFlightDefault.Count > IntNoOfRecordProcess));
                }
                else
                {
                    List<FlightDefault> TotalList1 = new List<FlightDefault>();

                    List<FlightDefault> InnerListstp0 = new List<FlightDefault>();


                    InnerListstp0.AddRange(objLstFlightDefault);//.Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                    TotalList1.AddRange(InnerListstp0);


                    objListListFlightDefault.Add(TotalList1);
                }



            }
            catch (Exception exc)
            {  // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + "public List<List<FlightDefault>> Get3DDataFromDefaultFlightView(List<FlightDefault> objLstListFlightDefault)";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }


            return objListListFlightDefault;
        }
        #endregion

        #region Extended Method based on New view Process Flight Default from View
        public List<FlightDefaultExt> GetDataFromDefaultFlightViewExt()
        {
            List<FlightDefaultExt> objLstflightDefaults = new List<FlightDefaultExt>();
            string strConnString = ConfigurationManager.ConnectionStrings["SQLConnForDB3"].ToString();
            SqlConnection Con = new SqlConnection(strConnString);
            Con.Open();

            try
            {





                #region column No for respective row to be using 
                //public string strFlightNo { get; set; }
                //public string strURLForChangeDefault { get; set; }
                //public string strDropDownType { get; set; }
                //public string strDefaultRowId { get; set; }
                //public string strOptionalRowId { get; set; }
                //  [Flightnumber]
                //,[TBPriceWeblink]
                //,[RateOption]
                #endregion

                string strQueryForPriceAndCost = "Select * from ext.vFlightDefaults";
                SqlCommand objSqlCommandForUpdatePrice = new SqlCommand(strQueryForPriceAndCost, Con);
                objSqlCommandForUpdatePrice.CommandType = System.Data.CommandType.Text;
                objSqlCommandForUpdatePrice.CommandText = strQueryForPriceAndCost;
                objSqlCommandForUpdatePrice.CommandTimeout = 360;
                SqlDataReader objSQLDataReader;

                objSQLDataReader = objSqlCommandForUpdatePrice.ExecuteReader();

                if (objSQLDataReader.HasRows)
                {
                    while (objSQLDataReader.Read())
                    {
                        FlightDefaultExt objTempFlightDefault = new FlightDefaultExt();
                        //objLstflightDefaults

                        objTempFlightDefault.strURLForChangeDefault = objSQLDataReader["TBPriceWeblink"].ToString();
                        objTempFlightDefault.strOptionalRowId = objSQLDataReader["OptionalRowID"].ToString();
                        objTempFlightDefault.strDefaultRowId = objSQLDataReader["DefaultRowID"].ToString();

                        objLstflightDefaults.Add(objTempFlightDefault);
                    }
                }

                Int32 intNoOfrecords = objLstflightDefaults.Count;
                // Log the Start of Process
                LibraryUtil.EmailBody("No of Records :" + intNoOfrecords.ToString());
                




            }
            catch (Exception exc)
            {
                //handle Exceptionhere
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public List<FlightDefault> GetDataFromDefaultFlightView()";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }


            return objLstflightDefaults;

        }
        #endregion

        #region Extended Method based on New view Get 3D object flightdefault data
        public List<List<FlightDefaultExt>> Get3DDataFromDefaultFlightViewExt()
        {
            List<List<FlightDefaultExt>> objListListFlightDefault = new List<List<FlightDefaultExt>>();
            try
            {
                List<FlightDefaultExt> objLstFlightDefault = GetDataFromDefaultFlightViewExt();
                Int32 IntNoOfRecordProcess = 0;
                Int32 IntNoOfthreads = 0;
                //IntNoOfRecordProcess = Convert.ToInt32(ConfigurationManager.AppSettings["NoOfRecordToProcess"].ToString());
                IntNoOfthreads = Convert.ToInt32(ConfigurationManager.AppSettings["NoOfThread"].ToString());
                if (objLstFlightDefault.Count > 50)
                {
                    IntNoOfRecordProcess = objLstFlightDefault.Count / IntNoOfthreads + 1;
                }


                if (IntNoOfRecordProcess > 0)
                {
                    do
                    {
                        if ((objListListFlightDefault.Count > 0) && (objLstFlightDefault.Count < IntNoOfRecordProcess))
                        {
                            IntNoOfRecordProcess = objLstFlightDefault.Count;
                        }
                        List<FlightDefaultExt> TotalList1 = new List<FlightDefaultExt>();

                        List<FlightDefaultExt> InnerListstp0 = new List<FlightDefaultExt>();


                        InnerListstp0.AddRange(objLstFlightDefault.Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.AddRange(InnerListstp0);
                        objLstFlightDefault.RemoveRange(0, IntNoOfRecordProcess);






                        objListListFlightDefault.Add(TotalList1);

                    } while (!(objListListFlightDefault[0].Count - objLstFlightDefault.Count > IntNoOfRecordProcess));
                }
                else
                {
                    List<FlightDefaultExt> TotalList1 = new List<FlightDefaultExt>();

                    List<FlightDefaultExt> InnerListstp0 = new List<FlightDefaultExt>();


                    InnerListstp0.AddRange(objLstFlightDefault);//.Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                    TotalList1.AddRange(InnerListstp0);


                    objListListFlightDefault.Add(TotalList1);
                }



            }
            catch (Exception exc)
            {  // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public List<List<FlightDefaultExt>> Get3DDataFromDefaultFlightViewExt())";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }


            return objListListFlightDefault;
        }
        #endregion
    }
}
