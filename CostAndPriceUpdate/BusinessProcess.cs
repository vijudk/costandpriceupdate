﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using System.Configuration;
using System.IO;
using Renci.SshNet;
using OpenQA.Selenium.Remote;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System.Diagnostics;
using System.Management;

namespace CostAndPriceUpdate
{
    public class BusinessProcess
    {

        #region Process CSV Files
        public List<List<string>> ProcessCSVFiles(string strCSVFilePath)
        {
            List<List<string>> TotalList = new List<List<string>>();

            try
            {
                using (var reader = new StreamReader(@strCSVFilePath))
                {
                    List<string> listA = new List<string>();
                    List<string> listB = new List<string>();

                    List<string> TBCostWeblink = new List<string>();
                    List<string> TBPriceWeblink = new List<string>();
                    List<string> RowID = new List<string>();
                    List<string> AdultCost = new List<string>();
                    List<string> AdultPrice = new List<string>();
                    List<string> UpdatedCost = new List<string>();
                    List<string> UpdatedPrice = new List<string>();
                    List<string> OutboundFlightDate = new List<string>();
                    List<string> OuboundFlightNumber = new List<string>();

                    #region column No for respective row to be using 
                    //TBCostWeblink       48
                    //TBPriceWeblink      49
                    //RowID       53
                    //AdultCost       42
                    //AdultPrice      47
                    //UpdatedCost     60
                    //UpdatedPrice        61
                    //OutboundFlightDate      58
                    //OuboundFlightNumber     59
                    #endregion

                    char[] CommaSap = new char[3];
                    CommaSap[0] = ' ';
                    CommaSap[1] = ',';
                    CommaSap[2] = ' ';


                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        line = CheckFieldEnclosed(line);
                        var values = line.Split(',');

                        if (values.Length > 0)
                        {

                            for (int i = 0; i < values.Length; i++)
                            {
                                //switch (values[i].ToLower())
                                switch (i)
                                {
                                    //case "tbcostweblink":
                                    case 47:
                                        TBCostWeblink.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "tbpriceweblink":
                                    case 48:
                                        TBPriceWeblink.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "rowid":
                                    case 52:
                                        RowID.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "adultcost":
                                    case 41:
                                        AdultCost.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "adultprice":
                                    case 46:
                                        AdultPrice.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "updatedcost":
                                    case 59:
                                        UpdatedCost.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "updatedprice":
                                    case 60:
                                        UpdatedPrice.Add(CheckPipeDoubleQuoteAndRemove(values[i]));
                                        break;
                                    //case "outboundflightdate":
                                    case 57:
                                        OutboundFlightDate.Add(values[i]);
                                        break;
                                    //case "ouboundflightnumber":
                                    case 58:
                                        OuboundFlightNumber.Add(values[i]);
                                        break;

                                }




                            }





                        }
                    }

                    TotalList.Add(TBPriceWeblink);
                    TotalList.Add(TBCostWeblink);
                    TotalList.Add(RowID);
                    TotalList.Add(AdultCost);
                    TotalList.Add(AdultPrice);
                    TotalList.Add(UpdatedCost);
                    TotalList.Add(UpdatedPrice);
                    TotalList.Add(OutboundFlightDate);
                    TotalList.Add(OuboundFlightNumber);

                }
            }
            catch (Exception exc)
            {
                //handle Exceptionhere
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " Occured in Process CSV File Mathod";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

            }
            Int32 intNoOfrecords = TotalList[0].Count;
            // Log the Start of Process
            LibraryUtil.EmailBody("No of Records : " + intNoOfrecords.ToString());
            return TotalList;
            

        }
        #endregion
        #region Below Method will check for comma in Quotes and replace it with Pipe
        public string CheckFieldEnclosed(string strInput)
        {
            // Check Double Quote and replace, to Pipe
            string strGetStringToReplace = strInput;
            string strReplceString = strInput;
            int intStartInd = 0;
            int intEndIndex = 0;
            int intStartPoint = 0;
            try
            {
                do
                {
                    intStartInd = strInput.IndexOf("\"", intStartPoint);
                    if (intStartInd >= 0)
                    {
                        intEndIndex = strInput.IndexOf("\"", intStartInd + 1);
                        strGetStringToReplace = strInput.Substring(intStartInd, intEndIndex - intStartInd);
                        strInput = strInput.Replace(strGetStringToReplace, strGetStringToReplace.Replace(',', '|'));
                        intStartPoint = intEndIndex + 1;
                    }
                } while (strInput.IndexOf("\"", intStartPoint) > 0);

            }
            catch (Exception exc)
            {
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " Occured in CheckFieldEnclosed(string strInput)";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }
            return strInput;

        }
        #endregion

        #region replace Pipe with Comma
        public string CheckPipeAndRemove(string strCheckPipeAndRemove)
        {
            string strReturn = strCheckPipeAndRemove.Replace('|', ',');

            return strReturn;

        }
        #endregion

        #region replace Pipe with Comma
        public string CheckPipeDoubleQuoteAndRemove(string strCheckPipeAndRemove)
        {
            string strReturn = strCheckPipeAndRemove.Replace('|', ',');
            strReturn = strCheckPipeAndRemove.Replace("\"", "");
            return strReturn;

        }
        #endregion

        #region Login for givin Application
        public ChromeDriver LoginToApp()
        {
            try
            {

                ChromeOptions options = new ChromeOptions();
                options.AcceptInsecureCertificates = true;
                options.AddArgument("--ignore-certificate-errors");
                options.AddArgument("--headless");
                options.AddArgument("--disable-gpu");
                options.AddArgument("--no-startup-window");

                var chromeDriverService = ChromeDriverService.CreateDefaultService();
                chromeDriverService.HideCommandPromptWindow = true;

                string strLoginURL = ConfigurationManager.AppSettings["TightBayURL"].ToString();

                var increatetimeout = TimeSpan.FromMinutes(3);


                var driver = new ChromeDriver(chromeDriverService, options, increatetimeout);





                driver.Manage().Window.Maximize();
                driver.Navigate().GoToUrl(@strLoginURL);

                var UserName = driver.FindElementById("Username");
                var Password = driver.FindElementById("Password");

                // ************** Live User Login
                //UserName.SendKeys("robot@newmarket.travel");
                //Password.SendKeys("Welcome1!");

                //***************** Staging User Login details
                UserName.SendKeys("vijay.kanaginhal@newmarketholidays.co.uk");
                Password.SendKeys("Welcome1!");

                var Login = driver.FindElementByName("button");

                Login.Click();
                LibraryUtil.LogActivities(": Stat of Login: " + DateTime.Now);
                var SessionForTiger = driver.SessionId;

                return driver;

            }
            catch (Exception exc)
            {
                // Handle to Error Log

                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " Application Login Page";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
                return null;

            }


        }
        #endregion

        #region Login for givin Application
        public ChromeDriver LoginToApp(string strOrderOfLogin)
        {
            try
            {

                ChromeOptions options = new ChromeOptions();
                options.AcceptInsecureCertificates = true;
                options.AddArgument("--ignore-certificate-errors");
                options.AddArgument("--headless");
                options.AddArgument("--disable-gpu");
                options.AddArgument("--no-startup-window");
                options.UnhandledPromptBehavior = UnhandledPromptBehavior.Accept;
                

                var chromeDriverService = ChromeDriverService.CreateDefaultService();
                chromeDriverService.HideCommandPromptWindow = true;

                string strLoginURL = ConfigurationManager.AppSettings["TightBayURL"].ToString();

                var increatetimeout = TimeSpan.FromMinutes(3);


                var driver = new ChromeDriver(chromeDriverService, options, increatetimeout);


                string strUserName = "vijay.kanaginhal@newmarketholidays.co.uk";
                string strPassword = "Welcome1!";



                driver.Manage().Window.Maximize();
                driver.Navigate().GoToUrl(@strLoginURL);

                var UserName = driver.FindElementById("Username");
                var Password = driver.FindElementById("Password");

                // ************** Live User Login
                //UserName.SendKeys("robot@newmarket.travel");
                //Password.SendKeys("Welcome1!");

                //***************** Staging User Login details
                //UserName.SendKeys("vijay.kanaginhal@newmarketholidays.co.uk");
                //Password.SendKeys("Welcome1!");

                // Get Username and password from web config, check for LIVE or prod from Config

                string strCheckStagingOrProd = ConfigurationManager.AppSettings["Staging"].ToString();
                if (strCheckStagingOrProd.ToLower().Contains("false"))
                {
                    strUserName = ConfigurationManager.AppSettings["ProductionLogin"].ToString();
                    strPassword = ConfigurationManager.AppSettings["ProductionPassword"].ToString();
                }

                UserName.SendKeys(strUserName);
                Password.SendKeys(strPassword);


                var Login = driver.FindElementByName("button");

                Login.Click();
                LibraryUtil.LogActivities(": Start of Login: for Process :" + strOrderOfLogin.ToString() + "@:" + DateTime.Now);
                var SessionForTiger = driver.SessionId;

                return driver;

            }
            catch (Exception exc)
            {
                // Handle to Error Log

                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " Application Login Page";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
                return null;

            }


        }
        #endregion
        #region Depricated Process Cost and Price update
        public Boolean ProcessCostPriceUpDate(ChromeDriver driver)
        {
            Boolean BoolRetVal = false;

            // Process Each Value from CSV File
            //********************************
            //TotalList.Add(TBPriceWeblink);
            //TotalList.Add(TBCostWeblink);
            //TotalList.Add(RowID);  cph_col2_cph_content_uiComponents_uxAdultPrice_0
            //TotalList.Add(AdultCost);
            //TotalList.Add(AdultPrice);
            //TotalList.Add(UpdatedCost); cph_main_cContractRows_uiContracts_txt_adult_cost_0; cph_main_uxSave
            //TotalList.Add(UpdatedPrice);
            //TotalList.Add(OutboundFlightDate);
            //TotalList.Add(OuboundFlightNumber);

            string strSubmitRowIdPrice = "cph_col2_cph_content_uxSave";

            string StrAdultCostControl = "cph_main_cContractRows_uiContracts_txt_adult_cost_0";
            string strSubmitCostPriceControl = "cph_main_uxSave";
            string strCSVFilePathWithFile = string.Empty;
            strCSVFilePathWithFile = ConfigurationManager.AppSettings["CSVFilePath"].ToString();

            List<List<string>> TotalList = new List<List<string>>();
            TotalList = ProcessCSVFiles(strCSVFilePathWithFile);
            int i = 0;
            //"On: " + DateTime.Now.ToString() + 
            //LibraryUtil.LogActivities(DateTime.Now + "No : " + i.ToString());
            foreach (List<string> LstEachLineVal in TotalList)
            {
                LibraryUtil.LogActivities(DateTime.Now + "No : " + i.ToString());
                LibraryUtil.LogActivities("---------------------------------");
                foreach (string EachLineVal in LstEachLineVal)
                {
                    //foreach (string strGoHyper in EachLineVal)
                    //{
                    if (EachLineVal.ToString().Contains("http"))
                    {


                        string strRowIdControl = "cph_col2_cph_content_uiComponents_uxAdultPrice_";
                        string strPriceURL = EachLineVal;
                        // For Staging Use Below Method
                        strPriceURL = ChangeStagingURL(strPriceURL);
                        LibraryUtil.LogActivities("PriceWebLink: " + strPriceURL.ToString());
                        //driver.Manage().Window.Maximize();
                        driver.Navigate().GoToUrl(strPriceURL);
                        //Thread.SpinWait(50000);
                        var InternalRowId = TotalList[2][i].ToString();
                        strRowIdControl = strRowIdControl + InternalRowId;
                        LibraryUtil.LogActivities("RowId: " + TotalList[2][i].ToString());
                        try
                        {
                            var CostPriceUpdate = driver.FindElementById(strRowIdControl);

                            IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
                            jse.ExecuteScript("arguments[0].focus", CostPriceUpdate);
                            CostPriceUpdate.Clear();
                            CostPriceUpdate.SendKeys(TotalList[3][i].ToString());
                            LibraryUtil.LogActivities("Price : " + TotalList[3][i].ToString());
                            var VarSubmit = driver.FindElementById(strSubmitRowIdPrice);
                            LibraryUtil.LogActivities("Price Submit Successfully");


                            jse.ExecuteScript("document.getElementById('cph_col2_cph_content_uxSave').focus();");
                            VarSubmit.Submit(); //.Click();

                            //var CostPriceUpdate = driver.FindElementById(strRowIdControl);

                            //CostPriceUpdate.Clear();
                            //CostPriceUpdate.SendKeys(TotalList[3][i].ToString());
                            //LibraryUtil.LogActivities("Price : " + TotalList[3][i].ToString());
                            //var VarSubmit = driver.FindElementById(strSubmitRowIdPrice);
                            //LibraryUtil.LogActivities("Price Submit Successfully");
                            //VarSubmit.Click(); //.Submit();
                        }
                        catch (Exception exc)
                        {
                            // Handle to Error Log
                            string StrErrorMessage = string.Empty;
                            string strStackTrace = string.Empty;
                            string strAreaOfError = string.Empty;

                            StrErrorMessage = exc.Message;
                            strStackTrace = exc.StackTrace.ToString();
                            strAreaOfError = exc.Source + " Occured in Web HyperLink URL Crome Drive";

                            LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

                        }

                        // ********************************
                        // Process Cost Update link

                        try
                        {
                            var StrCostWebLink = TotalList[1][i].ToString();
                            if (StrCostWebLink.ToString().Contains("http"))
                            {
                                StrCostWebLink = ChangeStagingURL(StrCostWebLink);
                                LibraryUtil.LogActivities("Cost URL: " + StrCostWebLink);
                                //driver.Manage().Window.Maximize();
                                driver.Navigate().GoToUrl(StrCostWebLink);
                                //Thread.SpinWait(50000);

                                var CostUpdateControl = driver.FindElementById(StrAdultCostControl);
                                CostUpdateControl.Clear();

                                CostUpdateControl.SendKeys(TotalList[4][i].ToString());
                                LibraryUtil.LogActivities("Cost : " + TotalList[4][i].ToString());
                                var VarSubmit = driver.FindElementById(strSubmitCostPriceControl);
                                VarSubmit.Submit();
                                LibraryUtil.LogActivities("Cost Submited Successfully: ");
                                LibraryUtil.LogActivities("---------------------------------");
                            }
                            else
                            {
                                // Write the Log and reason for Non http Link

                            }
                        }
                        catch (Exception exc)
                        {

                            // Handle to Error Log
                            string StrErrorMessage = string.Empty;
                            string strStackTrace = string.Empty;
                            string strAreaOfError = string.Empty;

                            StrErrorMessage = exc.Message;
                            strStackTrace = exc.StackTrace.ToString();
                            strAreaOfError = exc.Source + " Occured in Web HyperLink URL Crome Drive";

                            LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);


                        }





                    }
                    i++;


                }

            }

            driver.Close();
            LibraryUtil.LogActivities("End Of Process: " + DateTime.Now);


            return BoolRetVal;

        }
        #endregion

        #region Depricated Process Cost and Price update with Given 3 Dimension generic List
        public Boolean ProcessCostPriceUpDate(ChromeDriver driver, List<List<string>> objSlicedList)
        {
            Boolean BoolRetVal = false;


            try
            {
                // Process Each Value from CSV File
                //********************************
                //TotalList.Add(TBPriceWeblink);
                //TotalList.Add(TBCostWeblink);
                //TotalList.Add(RowID);  cph_col2_cph_content_uiComponents_uxAdultPrice_0
                //TotalList.Add(AdultCost);
                //TotalList.Add(AdultPrice);
                //TotalList.Add(UpdatedCost); cph_main_cContractRows_uiContracts_txt_adult_cost_0; cph_main_uxSave
                //TotalList.Add(UpdatedPrice);
                //TotalList.Add(OutboundFlightDate);
                //TotalList.Add(OuboundFlightNumber);

                string strSubmitRowIdPrice = "cph_col2_cph_content_uxSave";

                string StrAdultCostControl = "cph_main_cContractRows_uiContracts_txt_adult_cost_0";
                string strSubmitCostPriceControl = "cph_main_uxSave";
                string strCSVFilePathWithFile = string.Empty;
                strCSVFilePathWithFile = ConfigurationManager.AppSettings["CSVFilePath"].ToString();

                List<List<string>> TotalList = new List<List<string>>();
                TotalList = objSlicedList;
                //int i = 0;
                //"On: " + DateTime.Now.ToString() + 
                //LibraryUtil.LogActivities(DateTime.Now + "No : " + i.ToString());
                foreach (List<string> LstEachLineVal in TotalList)
                {
                    int i = 0;
                    LibraryUtil.LogActivities(DateTime.Now + "No : " + i.ToString());
                    LibraryUtil.LogActivities("---------------------------------");
                    foreach (string EachLineVal in LstEachLineVal)
                    {
                        //foreach (string strGoHyper in EachLineVal)
                        //{
                        if (EachLineVal.ToString().Contains("http"))
                        {


                            string strRowIdControl = "cph_col2_cph_content_uiComponents_uxAdultPrice_";
                            string strPriceURL = EachLineVal;
                            // For Staging Use Below Method
                            strPriceURL = ChangeStagingURL(strPriceURL);
                            LibraryUtil.LogActivities("PriceWebLink: " + strPriceURL.ToString());
                            //driver.Manage().Window.Maximize();
                            driver.Navigate().GoToUrl(strPriceURL);
                            //Thread.SpinWait(50000);
                            var InternalRowId = TotalList[2][i].ToString();
                            strRowIdControl = strRowIdControl + InternalRowId;
                            LibraryUtil.LogActivities("RowId: " + TotalList[2][i].ToString());
                            try
                            {
                                var CostPriceUpdate = driver.FindElementById(strRowIdControl);

                                IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
                                jse.ExecuteScript(strRowIdControl, CostPriceUpdate);
                                CostPriceUpdate.Clear();
                                CostPriceUpdate.SendKeys(TotalList[3][i].ToString());
                                LibraryUtil.LogActivities("Price : " + TotalList[3][i].ToString());
                                var VarSubmit = driver.FindElementById(strSubmitRowIdPrice);
                                LibraryUtil.LogActivities("Price Submit Successfully");


                                jse.ExecuteScript("document.getElementById('cph_col2_cph_content_uxSave').focus();");
                                VarSubmit.Submit(); //.Click();

                                //var CostPriceUpdate = driver.FindElementById(strRowIdControl);

                                //CostPriceUpdate.Clear();
                                //CostPriceUpdate.SendKeys(TotalList[3][i].ToString());
                                //LibraryUtil.LogActivities("Price : " + TotalList[3][i].ToString());
                                //var VarSubmit = driver.FindElementById(strSubmitRowIdPrice);
                                //LibraryUtil.LogActivities("Price Submit Successfully");
                                //VarSubmit.Click(); //.Submit();
                            }
                            catch (Exception exc)
                            {
                                // Handle to Error Log
                                string StrErrorMessage = string.Empty;
                                string strStackTrace = string.Empty;
                                string strAreaOfError = string.Empty;

                                StrErrorMessage = exc.Message;
                                strStackTrace = exc.StackTrace.ToString();
                                strAreaOfError = exc.Source + " Occured in Web HyperLink URL Crome Drive";

                                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

                            }

                            // ********************************
                            // Process Cost Update link

                            try
                            {
                                var StrCostWebLink = TotalList[1][i].ToString();
                                if (StrCostWebLink.ToString().Contains("http"))
                                {
                                    StrCostWebLink = ChangeStagingURL(StrCostWebLink);
                                    LibraryUtil.LogActivities("Cost URL: " + StrCostWebLink);
                                    //driver.Manage().Window.Maximize();
                                    driver.Navigate().GoToUrl(StrCostWebLink);
                                    //Thread.SpinWait(50000);

                                    var CostUpdateControl = driver.FindElementById(StrAdultCostControl);
                                    CostUpdateControl.Clear();

                                    CostUpdateControl.SendKeys(TotalList[4][i].ToString());
                                    LibraryUtil.LogActivities("Cost : " + TotalList[4][i].ToString());
                                    var VarSubmit = driver.FindElementById(strSubmitCostPriceControl);
                                    VarSubmit.Submit();
                                    LibraryUtil.LogActivities("Cost Submited Successfully: ");
                                    LibraryUtil.LogActivities("---------------------------------");
                                }
                                else
                                {
                                    // Write the Log and reason for Non http Link

                                }
                            }
                            catch (Exception exc)
                            {

                                // Handle to Error Log
                                string StrErrorMessage = string.Empty;
                                string strStackTrace = string.Empty;
                                string strAreaOfError = string.Empty;

                                StrErrorMessage = exc.Message;
                                strStackTrace = exc.StackTrace.ToString();
                                strAreaOfError = exc.Source + " Occured in Web HyperLink URL Crome Drive";

                                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);


                            }





                        }
                        i++;


                    }

                }

                driver.Close();
                LibraryUtil.LogActivities("End Of Process: " + DateTime.Now);
            }
            catch (Exception exc)
            {

                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " ProcessCostPriceUpDate(driver, obj3DimensionList[j])";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);


            }

            return BoolRetVal;

        }
        #endregion

        #region Process Cost and Price update with Given 3 Dimension generic List,with Unique Log No
        public Boolean ProcessCostPriceUpDate(ChromeDriver driver, List<List<string>> objSlicedList, string LogFileExtn)
        {
            Boolean BoolRetVal = false;


            try
            {


                // Process Each Value from CSV File
                //********************************
                //TotalList.Add(TBPriceWeblink);
                //TotalList.Add(TBCostWeblink);
                //TotalList.Add(RowID);  cph_col2_cph_content_uiComponents_uxAdultPrice_0
                //TotalList.Add(AdultCost);
                //TotalList.Add(AdultPrice);
                //TotalList.Add(UpdatedCost); cph_main_cContractRows_uiContracts_txt_adult_cost_0; cph_main_uxSave
                //TotalList.Add(UpdatedPrice);
                //TotalList.Add(OutboundFlightDate);
                //TotalList.Add(OuboundFlightNumber);

                string strSubmitRowIdPrice = "cph_col2_cph_content_uxSave";

                string StrAdultCostControl = "cph_main_cContractRows_uiContracts_txt_adult_cost_0";
                string strSubmitCostPriceControl = "cph_main_uxSave";
                string strCSVFilePathWithFile = string.Empty;
                strCSVFilePathWithFile = ConfigurationManager.AppSettings["CSVFilePath"].ToString();

                List<List<string>> TotalList = new List<List<string>>();
                TotalList = objSlicedList;
                //int i = 0;
                //"On: " + DateTime.Now.ToString() + 
                //LibraryUtil.LogActivities(DateTime.Now + "No : " + i.ToString());
                var objLock = new Object();

                //foreach (List<string> LstEachLineVal in TotalList)
                //{

                Int32 intExactSlicedArry = 0;
                intExactSlicedArry = Convert.ToInt32(LogFileExtn);
                int i = 0;

                //foreach (string EachLineVal in LstEachLineVal)
                //foreach (string EachLineVal in TotalList[intExactSlicedArry])
                //for (int NoOfRec = 0; NoOfRec < TotalList[intExactSlicedArry].Count; NoOfRec++)
                for (int NoOfRec = 0; NoOfRec < TotalList[0].Count; NoOfRec++)
                {
                    lock (Thread.CurrentThread)
                    {
                        LibraryUtil.LogActivities(DateTime.Now + "No :( " + i.ToString() + ") ", LogFileExtn);
                        LibraryUtil.LogActivities("---------------------------------", LogFileExtn);
                        //foreach (string strGoHyper in EachLineVal)
                        //{
                        if (TotalList[0][i].ToString().Contains("http"))
                        {



                            try
                            {
                                string strRowIdControl = "cph_col2_cph_content_uiComponents_uxAdultPrice_";
                                string strPriceURL = TotalList[0][i].ToString();
                                strPriceURL = ChangeStagingURL(strPriceURL);
                                LibraryUtil.LogActivities("PriceWebLink: " + strPriceURL.ToString(), LogFileExtn);
                                driver.Navigate().GoToUrl(strPriceURL);
                                var InternalRowId = TotalList[2][i].ToString();
                                strRowIdControl = strRowIdControl + InternalRowId;
                                LibraryUtil.LogActivities("RowId: " + TotalList[2][i].ToString(), LogFileExtn);
                                var CostPriceUpdate = driver.FindElementById(strRowIdControl);

                                IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
                                jse.ExecuteScript(strRowIdControl, CostPriceUpdate);
                                CostPriceUpdate.Clear();
                                CostPriceUpdate.SendKeys(TotalList[6][i].ToString());
                                LibraryUtil.LogActivities("Price : " + TotalList[6][i].ToString(), LogFileExtn);
                                var VarSubmit = driver.FindElementById(strSubmitRowIdPrice);
                                LibraryUtil.LogActivities("Price Submit Successfully", LogFileExtn);


                                //jse.ExecuteScript("document.getElementById('cph_col2_cph_content_uxSave').focus();");
                                //jse.ExecuteScript(strSubmitRowIdPrice, VarSubmit);
                                jse.ExecuteScript("arguments[0].click()", VarSubmit);
                                //VarSubmit.Submit(); //.Click();


                            }
                            catch (Exception exc)
                            {
                                // Handle to Error Log
                                string StrErrorMessage = string.Empty;
                                string strStackTrace = string.Empty;
                                string strAreaOfError = string.Empty;

                                StrErrorMessage = "Process No: (" + LogFileExtn + ")" + exc.Message + Environment.NewLine;
                                StrErrorMessage = StrErrorMessage + "ProblemURL: " + ChangeStagingURL(TotalList[0][i].ToString()) + Environment.NewLine;
                                StrErrorMessage = StrErrorMessage + "Problem Price : " + TotalList[6][i].ToString() + Environment.NewLine;
                                strStackTrace = exc.StackTrace.ToString();
                                strAreaOfError = exc.Source + " Occured in Web HyperLink URL Crome Drive";

                                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
                                LibraryUtil.LogActivities("Price Submit Problem", LogFileExtn);

                                // handle timed out Exception
                                if (StrErrorMessage.ToLower().Contains("timed out"))
                                {
                                    driver = LoginToApp(LogFileExtn);
                                    i--;
                                }
                                // handle Alert Exception 
                                if (StrErrorMessage.ToLower().Contains("unexpected alert"))
                                {
                                    //driver.SwitchTo().Alert().Accept();
                                    //driver = LoginToApp(LogFileExtn);
                                    //i--;
                                }
                            }

                            // ********************************
                            // Process Cost Update link

                            try
                            {
                                var StrCostWebLink = TotalList[1][i].ToString();
                                if (StrCostWebLink.ToString().Contains("http"))
                                {
                                    StrCostWebLink = ChangeStagingURL(StrCostWebLink);
                                    LibraryUtil.LogActivities("Cost URL: " + StrCostWebLink, LogFileExtn);
                                    //driver.Manage().Window.Maximize();
                                    driver.Navigate().GoToUrl(StrCostWebLink);
                                    //Thread.SpinWait(50000);


                                    var CostUpdateControl = driver.FindElementById(StrAdultCostControl);
                                    CostUpdateControl.Clear();

                                    CostUpdateControl.SendKeys(TotalList[5][i].ToString());
                                    LibraryUtil.LogActivities("Cost : " + TotalList[5][i].ToString(), LogFileExtn);
                                    var VarSubmit = driver.FindElementById(strSubmitCostPriceControl);
                                    VarSubmit.Submit();
                                    LibraryUtil.LogActivities("Cost Submited Successfully: ", LogFileExtn);
                                    LibraryUtil.LogActivities("---------------------------------", LogFileExtn);
                                }
                                else
                                {
                                    // Write the Log and reason for Non http Link

                                }
                            }
                            catch (Exception exc)
                            {

                                // Handle to Error Log
                                string StrErrorMessage = string.Empty;
                                string strStackTrace = string.Empty;
                                string strAreaOfError = string.Empty;

                                StrErrorMessage = "Process No: (" + LogFileExtn + ")" + exc.Message + Environment.NewLine;
                                StrErrorMessage = StrErrorMessage + "ProblemURL: " + ChangeStagingURL(TotalList[1][i].ToString()) + Environment.NewLine;
                                StrErrorMessage = StrErrorMessage + "Problem Cost : " + TotalList[5][i].ToString().ToString() + Environment.NewLine;
                                strStackTrace = exc.StackTrace.ToString();
                                strAreaOfError = exc.Source + " Occured in Web HyperLink URL Crome Drive";

                                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
                                LibraryUtil.LogActivities("Cost Submit Problem: ", LogFileExtn);
                                // handle timed out Exception
                                if (StrErrorMessage.ToLower().Contains("timed out"))
                                {
                                    driver = LoginToApp(LogFileExtn);
                                    i--;
                                }
                                // handle Alert Exception 
                                if (StrErrorMessage.ToLower().Contains("unexpected alert"))
                                {
                                    //driver.SwitchTo().Alert().Accept();
                                    //driver = LoginToApp(LogFileExtn);
                                    //i--;
                                }
                            }


                        }


                    }
                    i++;

                    //var objCurrentThread = Thread.CurrentThread.ManagedThreadId 


                }

                //}

                driver.Close();
                LibraryUtil.LogActivities("End Of Process: with following Thread " + LogFileExtn + "@: " + DateTime.Now);
                foreach (ClsHoldThreadstate objClsHoldThreadstate in CostAndPriceUpdate.objThreadStatusList)
                {
                    if (Thread.CurrentThread.ManagedThreadId == objClsHoldThreadstate.ThdList)
                    {
                        objClsHoldThreadstate.boolState = false;
                    }
                }

                driver.Quit();

                CostAndPriceUpdate.IntStatNoOfLoop--;
                // for mail generation

                CheckforEmailSend();
            }
            catch (Exception exc)
            {

                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " ProcessCostPriceUpDate(driver, obj3DimensionList[j])";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

                // report thisvia email

            }

            return BoolRetVal;

        }
        #endregion

        #region Process Cost and Price update with Given 3 Dimension generic List,with Unique Log No
        public Boolean ProcessCostPriceUpDateFromTable(ChromeDriver driver, List<List<string>> objSlicedList, string LogFileExtn)
        {
            Boolean BoolRetVal = false;


            try
            {


                // Process Each Value from CSV File
                //********************************
                //TotalList.Add(TBPriceWeblink);
                //TotalList.Add(TBCostWeblink);
                //TotalList.Add(RowID);  cph_col2_cph_content_uiComponents_uxAdultPrice_0
                //TotalList.Add(AdultCost);
                //TotalList.Add(AdultPrice);
                //TotalList.Add(UpdatedCost); cph_main_cContractRows_uiContracts_txt_adult_cost_0; cph_main_uxSave
                //TotalList.Add(UpdatedPrice);
                //TotalList.Add(OutboundFlightDate);
                //TotalList.Add(OuboundFlightNumber);

                string strSubmitRowIdPrice = "cph_col2_cph_content_uxSave";

                string StrAdultCostControl = "cph_main_cContractRows_uiContracts_txt_adult_cost_0";
                string strSubmitCostPriceControl = "cph_main_uxSave";
                string strCSVFilePathWithFile = string.Empty;
                strCSVFilePathWithFile = ConfigurationManager.AppSettings["CSVFilePath"].ToString();

                List<List<string>> TotalList = new List<List<string>>();
                TotalList = objSlicedList;
                var objLock = new Object();
                Int32 intExactSlicedArry = 0;
                intExactSlicedArry = Convert.ToInt32(LogFileExtn);
                int i = 0;

                //for (int NoOfRec = 0; NoOfRec < TotalList[intExactSlicedArry].Count; NoOfRec++)
                for (int NoOfRec = 0; NoOfRec < TotalList[0].Count; NoOfRec++)
                {
                    lock (Thread.CurrentThread)
                    {
                        LibraryUtil.LogActivities(DateTime.Now + "No :( " + i.ToString() + ") ", LogFileExtn);
                        LibraryUtil.LogActivities("---------------------------------", LogFileExtn);
                        //foreach (string strGoHyper in EachLineVal)
                        //{
                        if (TotalList[0][i].ToString().Contains("http"))
                        {



                            try
                            {
                                string strRowIdControl = "cph_col2_cph_content_uiComponents_uxAdultPrice_";
                                string strPriceURL = TotalList[0][i].ToString();
                                strPriceURL = ChangeStagingURL(strPriceURL);
                                LibraryUtil.LogActivities("PriceWebLink: " + strPriceURL.ToString(), LogFileExtn);
                                driver.Navigate().GoToUrl(strPriceURL);
                                var InternalRowId = TotalList[2][i].ToString();
                                strRowIdControl = strRowIdControl + InternalRowId;
                                LibraryUtil.LogActivities("RowId: " + TotalList[2][i].ToString(), LogFileExtn);
                                var CostPriceUpdate = driver.FindElementById(strRowIdControl);

                                IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
                                jse.ExecuteScript(strRowIdControl, CostPriceUpdate);
                                CostPriceUpdate.Clear();
                                CostPriceUpdate.SendKeys(TotalList[6][i].ToString());
                                LibraryUtil.LogActivities("Price : " + TotalList[6][i].ToString(), LogFileExtn);
                                var VarSubmit = driver.FindElementById(strSubmitRowIdPrice);
                                LibraryUtil.LogActivities("Price Submit Successfully", LogFileExtn);


                                //jse.ExecuteScript("document.getElementById('cph_col2_cph_content_uxSave').focus();");
                                //jse.ExecuteScript(strSubmitRowIdPrice, VarSubmit);
                                jse.ExecuteScript("arguments[0].click()", VarSubmit);
                                //VarSubmit.Submit(); //.Click();


                            }
                            catch (Exception exc)
                            {
                                // Handle to Error Log
                                string StrErrorMessage = string.Empty;
                                string strStackTrace = string.Empty;
                                string strAreaOfError = string.Empty;

                                StrErrorMessage = "Process No: (" + LogFileExtn + ")" + exc.Message + Environment.NewLine;
                                StrErrorMessage = StrErrorMessage + "ProblemURL: " + ChangeStagingURL(TotalList[0][i].ToString()) + Environment.NewLine;
                                StrErrorMessage = StrErrorMessage + "Problem Price : " + TotalList[6][i].ToString() + Environment.NewLine;
                                strStackTrace = exc.StackTrace.ToString();
                                strAreaOfError = exc.Source + " Occured in Web HyperLink URL Crome Drive";

                                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
                                LibraryUtil.LogActivities("Price Submit Problem", LogFileExtn);

                                // handle timed out Exception
                                if (StrErrorMessage.ToLower().Contains("timed out"))
                                {
                                    driver = LoginToApp(LogFileExtn);
                                    i--;
                                }
                                // handle Alert Exception 
                                if (StrErrorMessage.ToLower().Contains("unexpected alert"))
                                {
                                    //driver.SwitchTo().Alert().Accept();
                                    //driver = LoginToApp(LogFileExtn);
                                    //i--;
                                }
                            }

                            // ********************************
                            // Process Cost Update link

                            try
                            {
                                var StrCostWebLink = TotalList[1][i].ToString();
                                if (StrCostWebLink.ToString().Contains("http"))
                                {
                                    StrCostWebLink = ChangeStagingURL(StrCostWebLink);
                                    LibraryUtil.LogActivities("Cost URL: " + StrCostWebLink, LogFileExtn);
                                    //driver.Manage().Window.Maximize();
                                    driver.Navigate().GoToUrl(StrCostWebLink);
                                    //Thread.SpinWait(50000);


                                    var CostUpdateControl = driver.FindElementById(StrAdultCostControl);
                                    CostUpdateControl.Clear();

                                    CostUpdateControl.SendKeys(TotalList[5][i].ToString());
                                    LibraryUtil.LogActivities("Cost : " + TotalList[5][i].ToString(), LogFileExtn);
                                    var VarSubmit = driver.FindElementById(strSubmitCostPriceControl);
                                    VarSubmit.Submit();
                                    LibraryUtil.LogActivities("Cost Submited Successfully: ", LogFileExtn);
                                    LibraryUtil.LogActivities("---------------------------------", LogFileExtn);
                                }
                                else
                                {
                                    // Write the Log and reason for Non http Link

                                }
                            }
                            catch (Exception exc)
                            {

                                // Handle to Error Log
                                string StrErrorMessage = string.Empty;
                                string strStackTrace = string.Empty;
                                string strAreaOfError = string.Empty;

                                StrErrorMessage = "Process No: (" + LogFileExtn + ")" + exc.Message + Environment.NewLine;
                                StrErrorMessage = StrErrorMessage + "ProblemURL: " + ChangeStagingURL(TotalList[1][i].ToString()) + Environment.NewLine;
                                StrErrorMessage = StrErrorMessage + "Problem Cost : " + TotalList[5][i].ToString().ToString() + Environment.NewLine;
                                strStackTrace = exc.StackTrace.ToString();
                                strAreaOfError = exc.Source + " Occured in Web HyperLink URL Crome Drive";

                                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
                                LibraryUtil.LogActivities("Cost Submit Problem: ", LogFileExtn);
                                // handle timed out Exception
                                if (StrErrorMessage.ToLower().Contains("timed out"))
                                {
                                    driver = LoginToApp(LogFileExtn);
                                    i--;
                                }
                                // handle Alert Exception 
                                if (StrErrorMessage.ToLower().Contains("unexpected alert"))
                                {
                                    //driver.SwitchTo().Alert().Accept();
                                    //driver = LoginToApp(LogFileExtn);
                                    //i--;
                                }
                            }


                        }


                    }
                    i++;

                    //var objCurrentThread = Thread.CurrentThread.ManagedThreadId 

                   // create CSV file to insert mass value


                }

                //}

                driver.Close();
                LibraryUtil.LogActivities("End Of Process: with following Thread " + LogFileExtn + "@: " + DateTime.Now);
                foreach (ClsHoldThreadstate objClsHoldThreadstate in CostAndPriceUpdate.objThreadStatusList)
                {
                    if (Thread.CurrentThread.ManagedThreadId == objClsHoldThreadstate.ThdList)
                    {
                        objClsHoldThreadstate.boolState = false;
                    }
                }

                driver.Quit();

                CostAndPriceUpdate.IntStatNoOfLoop--;
                // for mail generation

                CheckforEmailSend();
            }
            catch (Exception exc)
            {

                // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " ProcessCostPriceUpDate(driver, obj3DimensionList[j])";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);

                // report thisvia email

            }

            return BoolRetVal;

        }
        #endregion

        public void CheckforEmailSend()
        {
            Boolean SendStatus = true;

            foreach (ClsHoldThreadstate objClsHoldThreadstate in CostAndPriceUpdate.objThreadStatusList)
            {
                if (objClsHoldThreadstate.boolState)
                {
                    SendStatus = false;
                    break;
                }

            }
            //NoOfThread 
            Int32 intNoOfThread = 0;
            intNoOfThread = Convert.ToInt32(ConfigurationManager.AppSettings["NoOfThread"].ToString());

            //if ((SendStatus) &&(CostAndPriceUpdate.IntStatNoOfLoop == 0))
            if ( CostAndPriceUpdate.IntStatNoOfLoop == 0)
            {
                // Log the Start of Process
                CostAndPriceUpdate.objThreadStatusList.Clear();
                CostAndPriceUpdate.IntStatNoOfLoop = 0;
                LibraryUtil.EmailBody("Process finished at: " + DateTime.Now);
                // call Static Email Method
                LibraryUtil.SendLogEmail("l");
                LibraryUtil.SendLogEmail("e");
                LibraryUtil.SendEmailBodyWithStatus();
            }
        }

        #region process CSV file and slice to process in multithread
        public List<List<List<string>>> ProcessCSVWith3Dimension()
        {
            List<List<List<string>>> ObjMultipleList = new List<List<List<string>>>();
            try
            {
                string strCSVFilePathWithFile = string.Empty;

                FTPProcess objFTPProcess = new FTPProcess();

                //strCSVFilePathWithFile = ConfigurationManager.AppSettings["CSVFilePath"].ToString();

                strCSVFilePathWithFile = objFTPProcess.ProcessFTPDownload();
                List<List<string>> TotalList = new List<List<string>>();

                BusinessProcess objBusinessProcess = new BusinessProcess();

                TotalList = objBusinessProcess.ProcessCSVFiles(strCSVFilePathWithFile);

                // Now based on count of records plan create multiple threads

                Int32 intNoOfRecords = TotalList[0].Count;




                Int32 IntNoOfRecordProcess = 0;
                Int32 IntNoOfthreads = 0;
                //IntNoOfRecordProcess = Convert.ToInt32(ConfigurationManager.AppSettings["NoOfRecordToProcess"].ToString());
                IntNoOfthreads = Convert.ToInt32(ConfigurationManager.AppSettings["NoOfThread"].ToString());
                if (TotalList[0].Count > 50)
                {
                    IntNoOfRecordProcess = TotalList[0].Count / IntNoOfthreads + 1;
                }
                //else
                //{

                //}

                //if (ObjMultipleList[0] TotalList[0].Count>20)

                if (IntNoOfRecordProcess > 0)
                {
                    do
                    {
                        if ((ObjMultipleList.Count > 0) && (TotalList[0].Count < IntNoOfRecordProcess))
                        {
                            IntNoOfRecordProcess = TotalList[0].Count;
                        }
                        List<List<string>> TotalList1 = new List<List<string>>();

                        List<string> InnerListstp0 = new List<string>();


                        InnerListstp0.AddRange(TotalList[0].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp0);
                        TotalList[0].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp1 = new List<string>();
                        InnerListstp1.AddRange(TotalList[1].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp1);
                        TotalList[1].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp2 = new List<string>();
                        InnerListstp2.AddRange(TotalList[2].Take(IntNoOfRecordProcess).ToArray());//.ToArray()
                        TotalList1.Add(InnerListstp2);
                        TotalList[2].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp3 = new List<string>();
                        InnerListstp3.AddRange(TotalList[3].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp3);
                        TotalList[3].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp4 = new List<string>();
                        InnerListstp4.AddRange(TotalList[4].Take(IntNoOfRecordProcess).ToArray());//.ToArray()
                        TotalList1.Add(InnerListstp4);
                        TotalList[4].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp5 = new List<string>();
                        InnerListstp5.AddRange(TotalList[5].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp5);
                        TotalList[5].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp6 = new List<string>();
                        InnerListstp6.AddRange(TotalList[6].Take(IntNoOfRecordProcess).ToArray());//.ToArray()
                        TotalList1.Add(InnerListstp6);
                        TotalList[6].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp7 = new List<string>();
                        InnerListstp7.AddRange(TotalList[7].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp7);
                        TotalList[7].RemoveRange(0, IntNoOfRecordProcess);
                        List<string> InnerListstp8 = new List<string>();
                        InnerListstp8.AddRange(TotalList[8].Take(IntNoOfRecordProcess).ToArray());//.ToArray();
                        TotalList1.Add(InnerListstp8);
                        TotalList[8].RemoveRange(0, IntNoOfRecordProcess);




                        ObjMultipleList.Add(TotalList1);

                    } while (!(ObjMultipleList[0][0].Count - TotalList[0].Count > IntNoOfRecordProcess));
                }
                else
                {
                    List<List<string>> TotalList1 = new List<List<string>>();

                    List<string> InnerListstp0 = new List<string>();


                    InnerListstp0.AddRange(TotalList[0].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp0);
                    TotalList[0].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp1 = new List<string>();
                    InnerListstp1.AddRange(TotalList[1].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp1);
                    TotalList[1].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp2 = new List<string>();
                    InnerListstp2.AddRange(TotalList[2].ToArray());//.ToArray()
                    TotalList1.Add(InnerListstp2);
                    TotalList[2].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp3 = new List<string>();
                    InnerListstp3.AddRange(TotalList[3].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp3);
                    TotalList[3].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp4 = new List<string>();
                    InnerListstp4.AddRange(TotalList[4].ToArray());//.ToArray()
                    TotalList1.Add(InnerListstp4);
                    TotalList[4].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp5 = new List<string>();
                    InnerListstp5.AddRange(TotalList[5].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp5);
                    TotalList[5].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp6 = new List<string>();
                    InnerListstp6.AddRange(TotalList[6].ToArray());//.ToArray()
                    TotalList1.Add(InnerListstp6);
                    TotalList[6].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp7 = new List<string>();
                    InnerListstp7.AddRange(TotalList[7].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp7);
                    TotalList[7].RemoveRange(0, IntNoOfRecordProcess);
                    List<string> InnerListstp8 = new List<string>();
                    InnerListstp8.AddRange(TotalList[8].ToArray());//.ToArray();
                    TotalList1.Add(InnerListstp8);
                    TotalList[8].RemoveRange(0, IntNoOfRecordProcess);

                    ObjMultipleList.Add(TotalList1);
                }




            }
            catch (Exception exc)
            {  // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public List<List<List<string>>> ProcessCSVWith3Dimension()";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }

            return ObjMultipleList;
        }
        #endregion

        #region This metthod is called only when testing dummy data ( Staging server)
        public string ChangeStagingURL(string strPriceURL)
        {
            string strRetURL = strPriceURL;

            try
            {
                string CheckStaging = ConfigurationManager.AppSettings["Staging"].ToString();
                if (CheckStaging.ToLower().Contains("true"))
                {
                    strRetURL = strPriceURL.Replace("production", "staging");
                }
            }
            catch (Exception exc)
            {  // Handle to Error Log
                string StrErrorMessage = string.Empty;
                string strStackTrace = string.Empty;
                string strAreaOfError = string.Empty;

                StrErrorMessage = exc.Message;
                strStackTrace = exc.StackTrace.ToString();
                strAreaOfError = exc.Source + " public string ChangeStagingURL(string strPriceURL)";

                LibraryUtil.LogExceptions(StrErrorMessage, strStackTrace, strAreaOfError);
            }
            return strRetURL;

        }
        #endregion

        #region Kill All webdriver.exe process
        public void KillProcessAndChildren(string p_name)
        {

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * From Win32_Process Where Name = '" + p_name + "'");

            ManagementObjectCollection moc = searcher.Get();
            foreach (ManagementObject mo in moc)
            {

                try
                {
                    KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
                }
                catch (ArgumentException)
                {
                    break;
                }
            }

        }
        #endregion

        #region KillProcess which are not n Use
        public void KillProcessAndChildren(int pid)
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * From Win32_Process Where ParentProcessID=" + pid);
            ManagementObjectCollection moc = searcher.Get();
            foreach (ManagementObject mo in moc)
            {

                try
                {
                    KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
                }
                catch
                {
                    break;
                }
            }

            try
            {
                Process proc = Process.GetProcessById(pid);

                proc.Kill();
            }
            catch (ArgumentException)
            {
                // Process already exited.
            }
        }
        #endregion

    }

    public class ClsHoldThreadstate
    {
        public int ThdList { set; get; }
        public Boolean boolState { set; get; }
    }
}
